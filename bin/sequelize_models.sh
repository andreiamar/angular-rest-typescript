#!/bin/bash

# generate sequelize models
sequelize model:create --name User --attributes 'email:string firstname:string lastname:string password:string salt:string user_type:string pref_language_id:integer'

sequelize model:create --name Persona --attributes 'user_id:integer nickname:string photo:string is_default:boolean'

sequelize model:create --name UserGroup --attributes 'name:string category:string'

sequelize model:create --name UserGroupMapping --attributes 'user_id:integer user_group_id:integer'

sequelize model:create --name Permission --attributes 'name:string function:string userbase_scope_id:integer'

sequelize model:create --name PermissionGroup --attributes 'name:string rank:integer group_type:string'

sequelize model:create --name PermissionMapping --attributes 'permission_group_id:integer permission_id:integer'

sequelize model:create --name UserPermissionMapping --attributes 'user_id:integer permission_group_id:integer'

sequelize model:create --name ApplicationFunction --attributes 'name:string'

sequelize model:create --name UserbaseScope --attributes 'name:string'

sequelize model:create --name UserbaseScopeOverride --attributes 'userbase_scope_id:integer user_group_id:integer'

sequelize model:create --name Article --attributes 'author_id:integer title:string body:string copyright:string publication_status:string language_id:string'
