import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from "@angular/forms";

import { AuthService } from 'ng2-ui-auth';
import { LoginModel } from '../../shared/models/login.model';
import { ErrorService } from '../../shared/services/error.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  submitted: boolean = false;
  formErrors = {
    'email': '',
    'password': ''
  };
  validationMessages = {
    'email': {
      'email': 'Invalid email.',
      'required': 'Email is required.',
    },
    'password': {
      'required': 'Password is required.',
    }
  };

  constructor(
    private auth: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private errorService: ErrorService
  ) {}


  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.form = this.fb.group({
      email: [null, [
        Validators.email,
        Validators.required,
      ]],
      password: [null, [
        Validators.required,
      ]]
    });
    this.form.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.form) { return; }
    const form = this.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    const user:LoginModel = new LoginModel(
      this.form.value.email,
      this.form.value.password
    );
    this.auth.login(user).subscribe({
      error: (err: any) => {
        console.log('Error: ', err, err.message);
        this.errorService.handleError(err)
      },
      complete: () => {
        // Get the redirect URL from our auth service
        // If no redirect has been set, use the default
        // let redirect = this.authService.redirectUrl || '/dashboard';
        let redirect = '/articles';
        // Set our navigation extras object
        // that passes on our global query params and fragment
        let navigationExtras: NavigationExtras = {
          preserveQueryParams: true,
          preserveFragment: true
        };

        // Redirect the user
        this.router.navigate([redirect], navigationExtras);
      }
    });
    // this.form.reset();
  }

  loginWithGoogle() {
    localStorage.removeItem('google_state');
    this.auth.authenticate('google')
      .subscribe({
        error: (err: any) => {
          console.log('>> Error: ', err);
          // this.errorService.handleError(err)
        },
        complete: () => this.router.navigateByUrl('articles')
      });
  }

  loginWithFacebook() {
    this.auth.authenticate('facebook')
      .subscribe({
        error: (err: any) => {
          console.log('>> Error: ', err);
          // this.errorService.handleError(err)
        },
        complete: () => this.router.navigateByUrl('articles')
      });
  }

}
