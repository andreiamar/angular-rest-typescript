import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from "@angular/forms";
import { CustomValidators } from 'ng2-validation';
import { AuthService } from 'ng2-ui-auth';

import { SignupModel } from '../../shared/models/signup.model';
import { ErrorService } from '../../shared/services/error.service';

const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form: FormGroup;

  submitted: boolean = false;
  redirectUrl: string = 'account/profile';

  formErrors = {
    'firstname': '',
    'lastname': '',
    'email': '',
    'password': ''
  };

  validationMessages = {
    'firstname': {
      'required': 'First name is required.',
    },
    'lastname': {
      'required': 'Last name is required.',
    },
    'email': {
      'email': 'Invalid email.',
      'required': 'Email is required.',
    },
    'password': {
      'required': 'Password is required.',
      'minlength': 'Password must be at least 6 characters long.',
      'maxlength': 'Password cannot be more than 32 characters long.',
    }
  };

  constructor(
    private auth: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private errorService: ErrorService
  ) {}

  ngOnInit() {
    this.buildForm();

    // email: [null, Validators.compose([Validators.required, CustomValidators.email])],
    // password: password,
    // confirmPassword: confirmPassword
  }

  buildForm() {
    this.form = this.fb.group({
      firstname: [null, Validators.required],
      lastname: [null, Validators.required],
      email: [null, [
        Validators.required,
        Validators.email
      ]],
      password: [null, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32),
      ]]
    });
    this.form.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.form) { return; }
    const form = this.form;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    console.log('signin up...');
    console.log(this.form);

    const user:SignupModel = new SignupModel(
      this.form.value.firstname,
      this.form.value.lastname,
      this.form.value.email,
      this.form.value.password
    );

    this.auth.signup(user)
      .subscribe({
          next: (response) => this.auth.setToken(response.json().token),
          error: (err: any) => this.errorService.handleError(err),
          complete: () => this.router.navigateByUrl(this.redirectUrl)
      });

    // console.log('user:', user);
    // this.authService.signup(user).subscribe(data => {
    //   this.router.navigateByUrl('/dashboard');
    // });

    // this.form.reset();
  }

  loginWithGoogle() {
    localStorage.removeItem('google_state');
    this.auth.authenticate('google')
      .subscribe({
        error: (err: any) => this.errorService.handleError(err),
        complete: () => this.router.navigateByUrl(this.redirectUrl)
      });
  }

  loginWithFacebook() {
    this.auth.authenticate('facebook')
      .subscribe({
        error: (err: any) => this.errorService.handleError(err),
        complete: () => this.router.navigateByUrl(this.redirectUrl)
      });
  }
}
