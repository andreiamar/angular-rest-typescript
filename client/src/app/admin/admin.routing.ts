import { Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { UserGroupsComponent } from './user-groups/user-groups.component';
import { AddPermissionComponent } from './permissions/add-permission.component';
import { EditPermissionComponent } from './permissions/edit-permission.component';
import { AddPermissionGroupComponent } from './permissions/add-permission-group.component';
import { EditPermissionGroupComponent } from './permissions/edit-permission-group.component';
import { AssignGroupPermissionComponent } from './permissions/assign-group-permission.component';
import { PermissionResolver } from './resolves/permission.resolver';
import { PermissionsResolver } from './resolves/permissions.resolver';
import { PermissionGroupResolver } from './resolves/permission-group.resolver';
import { PermissionGroupsResolver } from './resolves/permission-groups.resolver';
import { GroupPermissionsResolver } from './resolves/group-permissions.resolver';
import { UserGroupResolver } from './resolves/user-group.resolver';
import { UserGroupsResolver } from './resolves/user-groups.resolver';
import { UserGroupPermissionGroupsResolver } from './resolves/user-group-permission-groups.resolver';
import { UserGroupUsersResolver } from './resolves/user-group-users.resolver';
import { AddUsergroupComponent } from './user-groups/add-usergroup.component';
import { EditUsergroupComponent } from './user-groups/edit-usergroup.component';
import { AssignUserGroupPermissionsComponent } from './user-groups/assign-usergroup-permissions.component';
import { AssignUsergroupUsersComponent } from './user-groups/assign-usergroup-users.component';
import { ListUsersComponent } from './users/list.component';
import { EditUserComponent } from './users/edit.component';
import { UserResolver } from './resolves/user.resolver';

export const AdminRoutes: Routes = [{
  path: '',
  component: AdminComponent,
  children: [{
    path: 'permissions',
    // component: PermissionsComponent,
    children: [{
      path: '',
      component: PermissionsComponent,
      resolve: {
        permissions: PermissionsResolver,
        permissionGroups: PermissionGroupsResolver
      },
    }, {
      path: 'add',
      component: AddPermissionComponent
    }, {
      path: 'edit/:id',
      component: EditPermissionComponent,
      resolve: {
        permission: PermissionResolver
      }
    }, {
      path: 'add-group',
      component: AddPermissionGroupComponent
    }, {
      path: 'edit-group/:id',
      component: EditPermissionGroupComponent,
      resolve: {
        permissionGroup: PermissionGroupResolver
      }
    }, {
      path: 'assign-permissions/:id',
      component: AssignGroupPermissionComponent,
      resolve: {
        permissionGroup: PermissionGroupResolver,
        groupPermissions: GroupPermissionsResolver
      }
    }]
  }, {
    path: 'user-groups',
    children: [{
      path: '',
      component: UserGroupsComponent,
      resolve: {
        userGroups: UserGroupsResolver
      }
    }, {
      path: 'add',
      component: AddUsergroupComponent
    }, {
      path: 'edit/:id',
      component: EditUsergroupComponent,
      resolve: {
        userGroup: UserGroupResolver
      }
    }, {
      path: 'assign-permissions/:id',
      component: AssignUserGroupPermissionsComponent,
      resolve: {
        userGroup: UserGroupResolver,
        userGroupPermissionGroups: UserGroupPermissionGroupsResolver
      }
    }, {
      path: 'assign-users/:id',
      component: AssignUsergroupUsersComponent,
      resolve: {
        userGroup: UserGroupResolver,
        userGroupUsers: UserGroupUsersResolver
      }
    }]
  }, {
    path: 'users',
    children: [{
      path: '',
      component: ListUsersComponent
    }, {
      path: 'list/:page',
      component: ListUsersComponent
    }, {
      path: 'edit/:id',
      component: EditUserComponent,
      resolve: {
        user: UserResolver
      }
    }]
  }]
}];
