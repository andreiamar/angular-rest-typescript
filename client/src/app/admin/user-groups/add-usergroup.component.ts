import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from "@angular/forms";

import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-add-usergroup',
  templateUrl: './add-usergroup.component.html'
})
export class AddUsergroupComponent implements OnInit {
  myForm: FormGroup;
  submitted: boolean = false;

  formErrors = {
    'name': ''
  };

  validationMessages = {
    'name': {
      'required': 'Name is required.',
    }
  };

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private restApi: RestApi,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.myForm = this.fb.group({
      name: [null, Validators.required]
    });
    this.myForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.myForm) { return; }
    const form = this.myForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    const name = this.myForm.value.name;

    this.restApi.post('user-groups', {name:name})
      .subscribe({
        complete: () => {
          console.log('saved user group. navigating to /user-groups...');
          this.router.navigateByUrl('/admin/user-groups');
        }
      });
  }

}
