import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NotificationService } from '../../shared/services/notification.service';
import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-user-groups',
  templateUrl: './user-groups.component.html'
})
export class UserGroupsComponent implements OnInit {

  userGroups: any[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private restApi: RestApi,
    private notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.userGroups = this.route.snapshot.data['userGroups'];
  }

  reloadUserGroups() {
    this.restApi.get('user-groups')
      .map(res => res.json())
      .subscribe(result => {
        this.userGroups = result;
      })
  }

  onDelete(id:number, confirm:boolean) {
    if (confirm) {
      let delete$ = this.restApi.delete('user-groups', id)
        .map(res => res.json());

      delete$
        .subscribe(res => {
          this.notificationService.openSnackbar({message: `Item was deleted`, action: 'Close'});
          this.reloadUserGroups();
        });
    }
  }

}
