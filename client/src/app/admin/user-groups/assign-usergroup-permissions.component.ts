import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-assign-usergroup-permissions',
  templateUrl: './assign-usergroup-permissions.component.html'
})
export class AssignUserGroupPermissionsComponent implements OnInit {
  userGroup;
  userGroupPermissionGroups;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private restApi: RestApi
  ) { }

  ngOnInit() {
    this.userGroup = this.route.snapshot.data['userGroup'];
    this.userGroupPermissionGroups = this.route.snapshot.data['userGroupPermissionGroups'];
  }

  onSubmit() {
    let enabledPermissionGroups = this.userGroupPermissionGroups
      .filter(group => !!group.on)
      .map(group => group.id);

    this.restApi.put('usergroup-permissiongroups', this.userGroup.id, {permissionGroups: enabledPermissionGroups})
      .subscribe({
        complete: () => {
          this.router.navigateByUrl('/admin/user-groups');
        }
      });
  }

}
