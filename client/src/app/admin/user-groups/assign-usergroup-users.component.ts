import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-assign-usergroup-users',
  templateUrl: './assign-usergroup-users.component.html'
})
export class AssignUsergroupUsersComponent implements OnInit {
  userGroup;
  userGroupUsers;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private restApi: RestApi
  ) { }

  ngOnInit() {
    this.userGroup = this.route.snapshot.data['userGroup'];
    this.userGroupUsers = this.route.snapshot.data['userGroupUsers'];
  }

  onSubmit() {
    let enabledUsers = this.userGroupUsers
      .filter(user => !!user.on)
      .map(user => user.id);

    this.restApi.put('usergroup-users', this.userGroup.id, {users: enabledUsers})
      .subscribe({
        complete: () => {
          this.router.navigateByUrl('/admin/user-groups');
        }
      });
  }

}
