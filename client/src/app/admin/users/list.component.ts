import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PaginationInstance } from 'ngx-pagination';
import { Observable } from 'rxjs/Rx';

import { RestApi } from '../../shared/services/restapi.service';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list.component.html'
})
export class ListUsersComponent implements OnInit {
  users$: Observable<any[]>;
  loading: boolean;
  currentUrl: string;
  queryParams: any;

  columns = [
    { prop: 'firstname', name: 'First name' },
    { prop: 'lastname', name: 'Last name' },
    { prop: 'email', name: 'Email' },
    { name: 'Actions' }
  ];
  rows = [];

  public config: PaginationInstance = {
    id: 'custom',
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private restApi: RestApi,
    private notificationService: NotificationService,
  ) {}

  ngOnInit() {
    this.queryParams = {};
    this.route.params
      .subscribe((params:Params) => {
        console.log('list - route params', params);
        console.log('list - current url', this.router.url);
        if (params.page) {
          this.config.currentPage = +params.page;
        } else {
          this.config.currentPage = 1;
        }
        this.currentUrl = this.router.url;
        this.loadPage(this.config.currentPage);
      });

    this.loadPage(this.config.currentPage);
  }

  onPageChange(page: number) {
    let baseUrl = '/admin/users/list';
    console.log('onPageChange', baseUrl, page);
    this.router.navigate([baseUrl, page]);
  }

  onDelete(id:number, confirm:boolean) {
    if (confirm) {
      let delete$ = this.restApi.delete('users', id)
        .map(res => res.json());

      delete$
        .subscribe(res => {
          this.notificationService.openSnackbar({message: `Item was deleted`, action: 'Close'});
          this.onPageChange(1);
        });
    }
  }

  loadPage(page: number) {
    this.loading = true;
    let url = `users?page=${page}`;
    console.log('loading page', url)

    this.users$ = this.restApi.get(url)
      .map(res => res.json())
      .do(res => {
        this.config.currentPage = page;
        this.config.totalItems = res.total;
        this.loading = false;
        // console.log('got results', this.config, res.data)
        this.rows = res.data;
      })
      .map(res => res.data)
  }
}
