import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import {
  MdIconModule,
  MdCardModule,
  MdInputModule,
  MdButtonModule,
  MdToolbarModule,
  MdTabsModule,
  MdListModule,
  MdProgressBarModule,
  MdMenuModule,
  MdSlideToggleModule,
  MdSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxPaginationModule } from 'ngx-pagination';

import { RestApi } from '../shared/services/restapi.service';
import { ErrorService } from '../shared/services/error.service';
import { NotificationService } from '../shared/services/notification.service';
import { SharedModule } from '../shared/shared.module';

import { AdminComponent } from './admin.component';
import { AdminRoutes } from './admin.routing';
import { PermissionResolver } from './resolves/permission.resolver';
import { PermissionsResolver } from './resolves/permissions.resolver';
import { PermissionGroupResolver } from './resolves/permission-group.resolver';
import { PermissionGroupsResolver } from './resolves/permission-groups.resolver';
import { GroupPermissionsResolver } from './resolves/group-permissions.resolver';
import { UserGroupResolver } from './resolves/user-group.resolver';
import { UserGroupsResolver } from './resolves/user-groups.resolver';
import { UserGroupPermissionGroupsResolver } from './resolves/user-group-permission-groups.resolver';
import { UserGroupUsersResolver } from './resolves/user-group-users.resolver';
import { PermissionsComponent } from './permissions/permissions.component';
import { UserGroupsComponent } from './user-groups/user-groups.component';
import { AddPermissionComponent } from './permissions/add-permission.component';
import { EditPermissionComponent } from './permissions/edit-permission.component';
import { EditPermissionGroupComponent } from './permissions/edit-permission-group.component';
import { AddPermissionGroupComponent } from './permissions/add-permission-group.component';
import { AssignGroupPermissionComponent } from './permissions/assign-group-permission.component';
import { AddUsergroupComponent } from './user-groups/add-usergroup.component';
import { EditUsergroupComponent } from './user-groups/edit-usergroup.component';
import { AssignUserGroupPermissionsComponent } from './user-groups/assign-usergroup-permissions.component';
import { AssignUsergroupUsersComponent } from './user-groups/assign-usergroup-users.component';
import { ListUsersComponent } from './users/list.component';
import { EditUserComponent } from './users/edit.component';
import { UserResolver } from './resolves/user.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminRoutes),
    MaterialModule,
    MdIconModule,
    MdCardModule,
    MdInputModule,
    MdButtonModule,
    MdToolbarModule,
    MdTabsModule,
    MdListModule,
    MdProgressBarModule,
    MdMenuModule,
    MdSlideToggleModule,
    MdSelectModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [ AdminComponent, PermissionsComponent, UserGroupsComponent, AddPermissionComponent, EditPermissionComponent, EditPermissionGroupComponent, AddPermissionGroupComponent, AssignGroupPermissionComponent, AddUsergroupComponent, EditUsergroupComponent, AssignUserGroupPermissionsComponent, AssignUsergroupUsersComponent, ListUsersComponent, EditUserComponent ],
  providers: [ PermissionResolver, PermissionsResolver, PermissionGroupResolver, PermissionGroupsResolver, GroupPermissionsResolver, UserGroupResolver, UserGroupsResolver, UserGroupPermissionGroupsResolver, UserGroupUsersResolver, UserResolver, RestApi, ErrorService, NotificationService ]
})

export class AdminModule {}
