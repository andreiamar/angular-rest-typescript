import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { NotificationService } from '../../shared/services/notification.service';
import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html'
})
export class PermissionsComponent implements OnInit {

  permissions: any[];
  permissionGroups: any[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private restApi: RestApi,
    private notificationService: NotificationService,
  ) { }

  ngOnInit() {
    this.permissions = this.route.snapshot.data['permissions'];
    this.permissionGroups = this.route.snapshot.data['permissionGroups'];
  }

  reloadPermissions() {
    this.restApi.get('permissions')
      .map(res => res.json())
      .subscribe(result => {
        this.permissions = result;
      })
  }

  reloadPermissionGroups() {
    this.restApi.get('permission-groups')
      .map(res => res.json())
      .subscribe(result => {
        this.permissionGroups = result;
      })
  }

  onDelete(id:number, confirm:boolean) {
    if (confirm) {
      let delete$ = this.restApi.delete('permissions', id)
        .map(res => res.json());

      delete$
        .subscribe(res => {
          this.notificationService.openSnackbar({message: `Item was deleted`, action: 'Close'});
          this.reloadPermissions();
        });
    }
  }

  onDeleteGroup(id:number, confirm:boolean) {
    if (confirm) {
      let delete$ = this.restApi.delete('permission-groups', id)
        .map(res => res.json());

      delete$
        .subscribe(res => {
          this.notificationService.openSnackbar({message: `Item was deleted`, action: 'Close'});
          this.reloadPermissionGroups();
        });
    }
  }

}
