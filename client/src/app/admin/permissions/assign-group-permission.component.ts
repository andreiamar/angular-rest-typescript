import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-assign-group-permission',
  templateUrl: './assign-group-permission.component.html'
})
export class AssignGroupPermissionComponent implements OnInit {
  permissionGroup;
  groupPermissions;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private restApi: RestApi
  ) { }

  ngOnInit() {
    this.permissionGroup = this.route.snapshot.data['permissionGroup'];
    this.groupPermissions = this.route.snapshot.data['groupPermissions'];
  }

  onSubmit() {
    let enabledPermissions = this.groupPermissions
      .filter(permission => !!permission.on)
      .map(permission => permission.id);

    this.restApi.put('group-permissions', this.permissionGroup.id, {permissions: enabledPermissions})
      .subscribe({
        complete: () => {
          this.router.navigateByUrl('/admin/permissions');
        }
      });
  }

}
