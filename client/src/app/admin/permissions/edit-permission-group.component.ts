import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from "@angular/forms";

import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-edit-permission-group',
  templateUrl: './edit-permission-group.component.html'
})
export class EditPermissionGroupComponent implements OnInit {

  permissionGroup: any;

  myForm: FormGroup;
  submitted: boolean = false;
  formErrors = {
    'name': ''
  };
  validationMessages = {
    'name': {
      'required': 'Name is required.',
    }
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private restApi: RestApi,
  ) { }

  ngOnInit() {
    this.permissionGroup = this.route.snapshot.data['permissionGroup'];
    this.buildForm();
  }

  buildForm() {
    this.myForm = this.fb.group({
      name: [this.permissionGroup.name, Validators.required]
    });
    this.myForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.myForm) { return; }
    const form = this.myForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    const name = this.myForm.value.name;

    this.restApi.put('permission-groups', this.permissionGroup.id, {name:name})
      .subscribe({
        complete: () => {
          console.log('saved permission. navigating to /permissions...');
          this.router.navigateByUrl('/admin/permissions');
        }
      });
  }

}
