import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { RestApi } from '../../shared/services/restapi.service';


@Injectable()
export class UserGroupsResolver implements Resolve<any> {
  constructor(
    private restApi: RestApi
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.restApi.get('user-groups')
      .map(res => res.json());
  }
}
