import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from "@angular/forms";
import { JwtHttp, AuthService, SharedService } from 'ng2-ui-auth';

import { EditProfileModel } from '../../shared/models/editprofile.model';
import { RestApi } from '../../shared/services/restapi.service';
import { ErrorService } from '../../shared/services/error.service';

@Component({
  selector: 'app-account-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditProfileComponent implements OnInit {
  user: any;
  myForm: FormGroup;
  submitted: boolean = false;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'email': '',
    'password': ''
  };

  validationMessages = {
    'firstname': {
      'required': 'First name is required.',
    },
    'lastname': {
      'required': 'Last name is required.',
    },
    'email': {
      'email': 'Invalid email.',
      'required': 'Email is required.',
    },
    'password': {
      'required': 'Password is required.',
      'minlength': 'Password must be at least 6 characters long.',
      'maxlength': 'Password cannot be more than 32 characters long.',
    }
  };

  constructor(
    private http: JwtHttp,
    private auth: AuthService,
    private authShared: SharedService,
    private errorService: ErrorService,
    private restApi: RestApi,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.user = this.auth.getPayload();
    this.buildForm();
  }

  buildForm() {
    this.myForm = this.fb.group({
      firstname: [this.user.firstname, Validators.required],
      lastname: [this.user.lastname, Validators.required],
      email: [this.user.email, [
        Validators.required,
        Validators.email
      ]],
      password: [null, [
        Validators.minLength(6),
        Validators.maxLength(32),
      ]]
    });
    this.myForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.myForm) { return; }
    const form = this.myForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    const user:EditProfileModel = new EditProfileModel(
      this.myForm.value.firstname,
      this.myForm.value.lastname,
      this.myForm.value.email,
      this.myForm.value.password
    );

    this.restApi.put('profile', 'me', user)
      .subscribe({
        next: (res:Response) => { this.authShared.setToken(res) },
        complete: () => {
          this.router.navigateByUrl('/account/profile');
        }
      });
  }
}
