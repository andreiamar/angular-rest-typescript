import { Routes } from '@angular/router';

import { AccountComponent } from './account.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './editprofile/editprofile.component';
import { ProfileResolver } from './resolves/profile.resolver';;

export const AccountRoutes: Routes = [{
  path: '',
  redirectTo: 'profile',
  pathMatch: 'full',
}, {
  path: '',
  // component: AccountComponent
  children: [{
    path: 'profile',
    component: ProfileComponent,
    resolve: {
      profile: ProfileResolver
    }
  }, {
    path: 'profile/edit',
    component: EditProfileComponent,
    resolve: {
      profile: ProfileResolver
    }
  }]
}];
