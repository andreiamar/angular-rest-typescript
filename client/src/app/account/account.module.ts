import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MaterialModule, MdIconModule, MdCardModule,
  MdInputModule,
  MdButtonModule,
  MdToolbarModule,
  MdTabsModule,
  MdListModule,
  MdMenuModule,
  MdSlideToggleModule,
  MdSelectModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AccountComponent } from './account.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './editprofile/editprofile.component';
import { ProfileResolver } from './resolves/profile.resolver';
import { AccountRoutes } from './account.routing';

import { RestApi } from '../shared/services/restapi.service';
import { ErrorService } from '../shared/services/error.service';
import { NotificationService } from '../shared/services/notification.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AccountRoutes),
    MaterialModule,
    MdIconModule,
    MdCardModule,
    MdInputModule,
    MdButtonModule,
    MdToolbarModule,
    MdTabsModule,
    MdListModule,
    MdMenuModule,
    MdSlideToggleModule,
    MdSelectModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AccountComponent,
    ProfileComponent,
    EditProfileComponent
  ],
  providers: [
    ProfileResolver, RestApi, ErrorService, NotificationService
  ]
})

export class AccountModule {}
