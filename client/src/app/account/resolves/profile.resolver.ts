import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JwtHttp, AuthService } from 'ng2-ui-auth';


@Injectable()
export class ProfileResolver implements Resolve<any> {
  constructor(
    private auth: AuthService,
  ) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.auth.getPayload();
  }
}
