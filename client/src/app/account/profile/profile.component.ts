import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from "@angular/forms";
import { JwtHttp, AuthService } from 'ng2-ui-auth';

// import { ArticleModel } from '../../shared/models/article.model';
import { RestApi } from '../../shared/services/restapi.service';
import { ErrorService } from '../../shared/services/error.service';

@Component({
  selector: 'app-account-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;
  expiration: Date;

  constructor(
    private http: JwtHttp,
    private auth: AuthService,
    private errorService: ErrorService,
    private restApi: RestApi,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.route.data
      .subscribe(data => {
        // console.log('profile data', data)
        this.user = data.profile;
        this.expiration = this.auth.getExpirationDate();
        localStorage.removeItem('google_state');
      });
  }

  refresh() {
    this.http.refreshToken()
      .subscribe({
        error: (err: any) => this.errorService.handleError(err),
        complete: () => {
          this.expiration = this.auth.getExpirationDate();
          this.user = this.auth.getPayload();
        }
      });
  }

  linkGoogle() {
    this.auth.link('google')
      .subscribe({
        error: (err: any) => this.errorService.handleError(err),
        complete: () => {
          this.expiration = this.auth.getExpirationDate();
          this.user = this.auth.getPayload();
        }
      });
  }

  linkFacebook() {
    this.auth.link('facebook')
      .subscribe({
        error: (err: any) => this.errorService.handleError(err),
        complete: () => {
          this.expiration = this.auth.getExpirationDate();
          this.user = this.auth.getPayload();
        }
      });
  }
}
