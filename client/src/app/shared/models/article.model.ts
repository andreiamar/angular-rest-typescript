
export class ArticleModel {
  slug?:string
  constructor(
    public title: string,
    public body: string,
    public tags?: string[]
  ) {}
}
