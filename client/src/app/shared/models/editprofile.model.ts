
export class EditProfileModel {
  constructor(
    public firstname: string,
    public lastname: string,
    public email: string,
    public password: string,
  ) {}
}
