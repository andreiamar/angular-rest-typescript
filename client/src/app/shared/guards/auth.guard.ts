import { RouterStateSnapshot, ActivatedRouteSnapshot, Router, CanActivate, CanActivateChild } from '@angular/router';
import { AuthService } from 'ng2-ui-auth';
import { Injectable } from '@angular/core';
/**
 * Created by Ron on 03/10/2016.
 */
@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(private auth: AuthService, private router: Router) {}
    isAuthenticated() {
      return this.auth.isAuthenticated();
    }
    canActivate(
        next:  ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ) {
        if (this.auth.isAuthenticated()) {
          return true;
        }
        this.router.navigateByUrl('/login');
        return false;
    }
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      return this.canActivate(route, state);
    }
}
