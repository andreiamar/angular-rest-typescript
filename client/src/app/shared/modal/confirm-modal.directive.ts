import { Directive, HostListener, Input, Output, EventEmitter } from '@angular/core';

import { NotificationService, ModalMessage, ConfirmModalMessage } from '../services/notification.service';

@Directive({
  selector: '[confirm-modal]'
})
export class ConfirmModalDirective {

  constructor(
    private notificationService: NotificationService
  ) {}

  @Input('modal-title')
  title:string;

  @Input('modal-body')
  body:string;

  @Input('modal-ok-btn')
  okBtnText:string;

  @Output()
  confirm:EventEmitter<Boolean> = new EventEmitter<Boolean>();

  @HostListener('click')
  onClick() {
      this.notificationService.openConfirmModal({
        title: this.title || 'Title missing',
        body: this.body || 'Body missing',
        okBtnText: this.okBtnText || 'Confirm',
        confirm: this.confirm
      });
  }
}
