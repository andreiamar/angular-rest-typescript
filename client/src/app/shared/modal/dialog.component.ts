import { Component, Optional, Input } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';

@Component({
  template: `
  <div class="app-dialog-header">
    <h5 class="mt-0">{{ title }}</h5>
  </div>

  <div class="app-dialog-body" [innerHTML]="body"></div>

  <div class="app-dialog-footer">
      <button md-button type="submit" *ngIf="okBtn" color="accent" (click)="dialogRef.close(true)" class="mat-accent mat-raised-button"><i class="fa fa-check fa-2x" aria-hidden="true"></i>{{okBtn}}</button>

      <button md-raised-button type="button" (click)="dialogRef.close(false)" class="mat-raised-button"><i class="fa fa-check fa-2x" aria-hidden="true"></i>{{closeBtn}}</button>
  </div>
  `,
})
export class DialogComponent {

  @Input()
  body: string;

  title: string;

  constructor(
    @Optional() public dialogRef: MdDialogRef<DialogComponent>
  ) { }
}
