import { Directive, HostListener, Input } from '@angular/core';

import { NotificationService, ModalMessage } from '../services/notification.service';

@Directive({
  selector: '[show-modal]'
})
export class ShowModalDirective {

  constructor(
    private notificationService: NotificationService
  ) {}

  @Input('modal-title')
  title:string;

  @Input('modal-body')
  body:string;

  @HostListener('click')
  onClick() {
      this.notificationService.openModal({
        title: this.title || 'Title missing',
        body: this.body || 'Body missing'
      });
  }
}
