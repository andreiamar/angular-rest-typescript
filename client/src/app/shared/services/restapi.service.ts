import { Injectable } from '@angular/core';
import { Response } from "@angular/http";
import { Observable } from "rxjs/Observable";

import { environment } from '../../../environments/environment';
import { ErrorService } from './error.service';
import { JwtHttp, AuthService, SharedService } from 'ng2-ui-auth';

import { environment as env } from '../../../environments/environment';
const baseUrl = env.apiBaseUrl;

@Injectable()
export class RestApi {
  constructor(
    private http: JwtHttp,
    private errorService: ErrorService
  ) {}

  get(resource:string, id?:number) {
    let resourcePath = resource + (id ? `/${id}` : '');
    return this.http.get(`${env.apiBaseUrl}${resourcePath}`)
      .catch(err => this.errorService.handleError(err));
  }
  post(resource:string, postData:any) {
    return this.http.post(`${env.apiBaseUrl}${resource}`, JSON.stringify(postData))
      .catch(err => this.errorService.handleError(err));
  }
  put(resource:string, id:any, postData:any) {
    return this.http.put(`${env.apiBaseUrl}${resource}/${id}`, JSON.stringify(postData))
      .catch(err => this.errorService.handleError(err));
  }
  delete(resource:string, id:number) {
    return this.http.delete(`${env.apiBaseUrl}${resource}/${id}`)
      .catch(err => this.errorService.handleError(err));
  }
}
