import { Injectable, EventEmitter } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig, MdSnackBar, MdSnackBarRef, SimpleSnackBar } from '@angular/material';

import { DialogComponent } from '../modal/dialog.component';

@Injectable()
export class NotificationService {
  errorOccurred = new EventEmitter<Error>();
  dialogRef: MdDialogRef<DialogComponent>;
  lastDialogResult: string;
  config: MdDialogConfig = {
    disableClose: false,
    width: '',
    height: '',
    position: {
      top: '',
      bottom: '',
      left: '',
      right: ''
    }
  };
  snackbarRef: MdSnackBarRef<SimpleSnackBar>;

  constructor(
    private dialog: MdDialog,
    private snackbar: MdSnackBar
  ) {}

  openModal(message:ModalMessage) {
    this.dialogRef = this.dialog.open(DialogComponent, this.config);

    this.dialogRef.componentInstance['title'] = message.title;
    this.dialogRef.componentInstance['body'] = message.body;
    this.dialogRef.componentInstance['closeBtn'] = 'Close';

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('lastDialogResult', result);
      this.lastDialogResult = result;
    })
  }

  openConfirmModal(message:ConfirmModalMessage) {
    let config = new MdDialogConfig();
    this.dialogRef = this.dialog.open(DialogComponent, config);
    let closeBtnText = message.closeBtnText || 'Cancel';

    this.dialogRef.componentInstance['title'] = message.title;
    this.dialogRef.componentInstance['body'] = message.body;
    this.dialogRef.componentInstance['okBtn'] = message.okBtnText;
    this.dialogRef.componentInstance['closeBtn'] = closeBtnText;

    this.dialogRef.afterClosed().subscribe(boolResult => {
      console.log('openConfirmModal -> lastDialogResult', boolResult);
      this.lastDialogResult = boolResult;
      message.confirm.emit(boolResult);
    })
  }

  openSnackbar(message:SnackbarMessage) {
    this.snackbarRef = this.snackbar.open(message.message, message.action);

    this.snackbarRef.afterDismissed().subscribe(() => {
      console.log('The snack-bar was dismissed');
    });

    this.snackbarRef.onAction().subscribe(() => {
      console.log('The snack-bar action was triggered!');
    });
  }

  showError(err:string) {
    this.openModal({title:'Error', body:err});
  }
}

export interface ModalMessage {
  title: string;
  body: string;
}

export interface ConfirmModalMessage {
  title: string;
  body: string;
  okBtnText: string;
  closeBtnText?: string;
  confirm: EventEmitter<Boolean>;
}

// https://material.angular.io/components/component/snack-bar
export interface SnackbarMessage {
  message: string;
  action: string;
}
