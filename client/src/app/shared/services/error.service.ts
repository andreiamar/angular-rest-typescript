import { Injectable } from '@angular/core';
import { Response } from "@angular/http";
import { Observable } from "rxjs/Observable";

import { environment } from '../../../environments/environment';
import { NotificationService } from './notification.service';

@Injectable()
export class ErrorService {
  protected defaultErrMessage: string = 'API Request Error!';

  constructor(
    private notificationService: NotificationService
  ) {}

  handleError(error: any) {
    console.log('error:handleError', typeof error, error instanceof Response, error);
    let errMessage: string;
    if (error instanceof Response) {
      // check if the json response contains a 'message' property
      if (error['_body']) {
        try {
          let body = JSON.parse(error['_body']);
          if (body['error']) {
            errMessage = body['error'];
          }
          else if (body['message']) {
            errMessage = body['message'];
          }
        }
        catch (e) {}
      }
      // if not, show the error code and status
      if (!errMessage) {
        let body = error.json() || '';
        let err = body.error || JSON.stringify(body);
        if (err.status && err.statusText) {
          errMessage = `${err.status} - ${err.statusText || ''} ${err}`;
        } else {
          errMessage = this.defaultErrMessage;
        }
      }
    } else {
      errMessage = error.message ? error.message : error.toString();
    }
    // inform the user of the error
    this.notificationService.showError(errMessage);
    return Observable.throw(errMessage);
  }
}
