import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';

import { ShowModalDirective } from './modal/show-modal.directive';
import { ConfirmModalDirective } from './modal/confirm-modal.directive';
import { DialogComponent } from './modal/dialog.component';

@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ShowModalDirective,
    ConfirmModalDirective,
    DialogComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    ShowModalDirective,
    ConfirmModalDirective,
    DialogComponent,
   ],
   entryComponents: [DialogComponent],
   providers: [ MenuItems ]
})

export class SharedModule {
 //  static forRoot(): ModuleWithProviders {
 //   return {
 //     ngModule: SharedModule,
 //     providers: [ AuthGuard ]
 //   };
 // }
}
