import { Injectable } from '@angular/core';
import { AuthService } from 'ng2-ui-auth';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const ADMINMENUITEMS = [
  {
    state: 'admin',
    name: 'ADMIN',
    type: 'sub',
    icon: 'apps',
    children: [
      {state: 'permissions', name: 'PERMISSIONS'},
      {state: 'user-groups', name: 'USER GROUPS'},
      {state: 'users', name: 'USERS'}
    ]
  }
];
const MENUITEMS = [
  {
    state: 'account',
    name: 'ACCOUNT',
    type: 'link',
    icon: 'explore'
  },
  {
    state: 'articles',
    name: 'ARTICLES',
    type: 'link',
    icon: 'explore'
  },
  // {
  //   state: 'tags',
  //   name: 'TAGS',
  //   type: 'link',
  //   icon: 'explore'
  // },
];

@Injectable()
export class MenuItems {
  constructor(
    private auth: AuthService,
  ) {}

  getAll(): Menu[] {
    let payload = this.auth.getPayload();
    let showAdminMenu = payload && payload.permissions && payload.permissions.indexOf('Admin-Functions') > -1;
    let menuitems = MENUITEMS.slice();
    if (showAdminMenu) {
      menuitems = menuitems.concat(ADMINMENUITEMS);
    }
    return menuitems;
  }

  add(menu: Menu) {
    MENUITEMS.push(menu);
  }
}
