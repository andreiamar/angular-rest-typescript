import { Routes } from '@angular/router';

import { AppLayoutComponent } from './layouts/app/app-layout.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthGuard } from './shared/guards/auth.guard';

export const AppRoutes: Routes = [{
  path: '',
  redirectTo: 'articles',
  pathMatch: 'full',
}, {
  path: '',
  component: AdminLayoutComponent,
  children: [{
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule',
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ],
  }]
}, {
  path: '',
  component: AppLayoutComponent,
  children: [{
    path: 'account',
    loadChildren: './account/account.module#AccountModule',
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ],
  }, {
    path: 'articles',
    loadChildren: './articles/articles.module#ArticlesModule',
  }]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [{
    // path: 'session',
    path: '',
    loadChildren: './session/session.module#SessionModule'
  }]
}, {
  path: '**',
  redirectTo: '404'
}];
