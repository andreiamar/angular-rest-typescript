import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';

import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { environment } from '../environments/environment';
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AppLayoutComponent } from './layouts/app/app-layout.component';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { SharedModule } from './shared/shared.module';

// import services
import { ErrorService } from './shared/services/error.service';
import { NotificationService } from './shared/services/notification.service';

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

// https://github.com/sahat/satellizer/
import { Ng2UiAuthModule, CustomConfig } from 'ng2-ui-auth';
export class CottusAuthConfig extends CustomConfig {
    defaultHeaders = {'Content-Type': 'application/json'};
    baseUrl = environment.apiBaseUrl;
    refreshUrl = environment.apiBaseUrl + 'auth/refresh';
    providers = {
      google: { clientId: environment.googleClientID },
      facebook: { clientId: environment.facebookClientID },
    };
}
import { AuthGuard } from './shared/guards/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    AppLayoutComponent,
    AdminLayoutComponent,
    AuthLayoutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    // SharedModule.forRoot(),
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }),
    MaterialModule,
    FlexLayoutModule,
    Ng2UiAuthModule.forRoot(CottusAuthConfig),
  ],
  providers: [AuthGuard, ErrorService, NotificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
