import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { ArticleModel } from '../../shared/models/article.model';
import { RestApi } from '../../shared/services/restapi.service';

@Injectable()
export class ArticleResolver implements Resolve<ArticleModel> {
  constructor(
    private restApi: RestApi
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ArticleModel> {
    return this.restApi.get('articles', route.params['id'])
      .map(res => res.json());
  }
}
