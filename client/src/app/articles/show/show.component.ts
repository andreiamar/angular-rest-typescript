import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService } from 'ng2-ui-auth';

import { NotificationService } from '../../shared/services/notification.service';
import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {
  id: number;
  article: any;
  canEdit: boolean;
  returnUrl: string;
  private sub: any;
  hasEditArticlePermission: boolean;
  hasDeleteArticlePermission: boolean;

  constructor(
    private restApi: RestApi,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService,
    private notificationService: NotificationService,
  ) { }

  ngOnInit() {
    // this.route.params
    //   .switchMap((params:Params) => ).subscribe(params => {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      if (params.returnUrl) {
        this.returnUrl = params.returnUrl;
      } else {
        this.returnUrl = '/articles';
      }
    });

    this.route.data
      .subscribe(data => {
        this.article = data.article;

        let payload = this.auth.getPayload();
        this.canEdit = payload && payload.sub && data.article.user_id === payload.sub;

        this.hasEditArticlePermission = payload && payload.permissions && (payload.permissions.indexOf('Edit-All-Articles') > -1 || (payload.permissions.indexOf('Edit-Own-Article') > -1 && data.article.user_id===payload.sub));

        this.hasDeleteArticlePermission = payload && payload.permissions && (payload.permissions.indexOf('Delete-All-Articles') > -1 || (payload.permissions.indexOf('Delete-Own-Article') > -1 && data.article.user_id===payload.sub));
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  onDelete(articleId:number, confirm:boolean) {
    console.log('onDelete', articleId, confirm);
    if (confirm) {
      let delete$ = this.restApi.delete('articles', articleId)
        .map(res => res.json());

      delete$
        .subscribe(res => {
          this.notificationService.openSnackbar({message: `Article was deleted`, action: 'Close'});
          this.router.navigateByUrl('/articles');
        });
    }
  }
}
