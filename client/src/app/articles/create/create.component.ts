import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from "@angular/forms";
import * as Quill from 'quill';

import { ArticleModel } from '../../shared/models/article.model';
import { RestApi } from '../../shared/services/restapi.service';
import { ErrorService } from '../../shared/services/error.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  myForm: FormGroup;
  submitted: boolean = false;
  formErrors = {
    'title': '',
    'body': '',
  };
  validationMessages = {
    'title': {
      'required': 'Title is required.',
    },
    'Body': {
      'required': 'Body is required.',
    },
  };
  tags:string[]

  constructor(
    private errorService: ErrorService,
    private restApi: RestApi,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.buildForm();

    const quill = new Quill('#create-article__body', {
      modules: {
        toolbar: {
          container: '#toolbar-toolbar'
        }
      },
      placeholder: 'Article body...',
      theme: 'snow'
    });

    quill.on('text-change', (delta, oldDelta, source) => {
      let text = quill.getText();
      this.myForm.controls['body'].setValue(text);
      this.onValueChanged();
    });
  }

  buildForm() {
    this.myForm = this.fb.group({
      title: [null, Validators.required],
      body: [null, Validators.required],
    });
    this.myForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.myForm) { return; }
    const form = this.myForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    const article:ArticleModel = new ArticleModel(
      this.myForm.value.title,
      this.myForm.value.body,
      this.tags
    );
    this.restApi.post('articles', article)
      .subscribe({
        error: (err:any) => this.errorService.handleError(err),
        complete: () => {
          this.router.navigateByUrl('/articles');
        }
      });
  }

  onTagsChanged(tags) {
    this.tags = tags;
  }
}
