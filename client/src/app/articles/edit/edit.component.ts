import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from "@angular/forms";
import * as Quill from 'quill';

import { ArticleModel } from '../../shared/models/article.model';
import { RestApi } from '../../shared/services/restapi.service';
import { ErrorService } from '../../shared/services/error.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  id: number;
  article: any;
  myForm: FormGroup;
  submitted: boolean = false;
  formErrors = {
    'title': '',
    'body': '',
  };
  validationMessages = {
    'title': {
      'required': 'Title is required.',
    },
    'Body': {
      'required': 'Body is required.',
    },
  };
  tags:string[]
  backUrl:string

  constructor(
    private errorService: ErrorService,
    private restApi: RestApi,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.route.data
      .subscribe(data => {
        this.article = data.article;
        this.setTags();
        this.setBackUrl();
        this.buildForm();

        const quill = new Quill('#edit-article__body', {
          modules: {
            toolbar: {
              container: '#toolbar-toolbar'
            }
          },
          placeholder: 'Article body...',
          theme: 'snow'
        });
        quill.on('text-change', (delta, oldDelta, source) => {
          let text = quill.getText();
          this.myForm.controls['body'].setValue(text);
          this.onValueChanged();
        });
        quill.setText(this.article.body);
      });
  }

  setTags() {
    if (this.article.tags) {
      this.tags = this.article.tags.map(tagMapping => {
        return tagMapping.tag.name;
      });
    }
  }

  setBackUrl() {
    this.backUrl = `/articles/${this.article.id}/${this.article.slug}`;
  }

  buildForm() {
    this.myForm = this.fb.group({
      title: [this.article.title, Validators.required],
      body: [this.article.body, Validators.required],
    });
    this.myForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }

  onValueChanged(data?: any) {
    if (!this.myForm) { return; }
    const form = this.myForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  onSubmit() {
    const article:ArticleModel = new ArticleModel(
      this.myForm.value.title,
      this.myForm.value.body,
      this.tags
    );
    const articleId = this.article.id;
    let result:any;
    this.restApi.put('articles', articleId, article)
      .subscribe({
        next: (data) => {
          if (data) {
            result = data.json();
          }
        },
        error: (err:any) => this.errorService.handleError(err),
        complete: () => {
          if (result) {
            this.router.navigateByUrl(`/articles/${articleId}/${result.slug}`);
          } else {
            this.router.navigateByUrl(`/articles`);
          }
        }
      });
  }

  onTagsChanged(tags) {
    this.tags = tags;
  }

}
