import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  MaterialModule, MdInputModule, MdChipsModule, MdIconModule, MdCardModule, MdButtonModule, MdListModule, MdProgressBarModule, MdMenuModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxPaginationModule } from 'ngx-pagination';
import { SharedModule } from '../shared/shared.module';
import { AuthGuard } from '../shared/guards/auth.guard';

import { ArticlesComponent } from './articles.component';
import { ArticlesRoutes } from './articles.routing';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
import { ShowComponent } from './show/show.component';
import { EditComponent } from './edit/edit.component';
import { ArticleResolver } from './resolves/article.resolver';

import { RestApi } from '../shared/services/restapi.service';
import { ErrorService } from '../shared/services/error.service';
import { NotificationService } from '../shared/services/notification.service';
import { TagsComponent } from './tags/tags.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ArticlesRoutes),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MdInputModule,
    MdChipsModule,
    MdIconModule,
    MdCardModule,
    MdButtonModule,
    MdListModule,
    MdProgressBarModule,
    MdMenuModule,
    ChartsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FlexLayoutModule
  ],
  declarations: [ ArticlesComponent, CreateComponent, ListComponent, ShowComponent, EditComponent, TagsComponent ],
  providers: [ ArticleResolver, RestApi, ErrorService, NotificationService, AuthGuard ]
})

export class ArticlesModule {}
