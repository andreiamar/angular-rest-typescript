import { Routes } from '@angular/router';

import { ArticlesComponent } from './articles.component';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
import { ShowComponent } from './show/show.component';
import { EditComponent } from './edit/edit.component';
import { ArticleResolver } from './resolves/article.resolver';
import { AuthGuard } from '../shared/guards/auth.guard';

export const ArticlesRoutes: Routes = [{
  path: '',
  // component: ArticlesComponent
  children: [
    {
      path: '',
      component: ListComponent
    },
    {
      path: 'list/:page',
      component: ListComponent
    },
    {
      path: 'create',
      component: CreateComponent,
      canActivate: [ AuthGuard ]
    },
    {
      path: 'edit/:id',
      component: EditComponent,
      resolve: {
        article: ArticleResolver
      },
      canActivate: [ AuthGuard ]
    },
    {
      path: 'tag/:tag',
      component: ListComponent
    },
    {
      path: 'tag/:tag/:page',
      component: ListComponent
    },
    {
      path: ':id/:slug',
      component: ShowComponent,
      resolve: {
        article: ArticleResolver
      }
    },
  ]
}];
