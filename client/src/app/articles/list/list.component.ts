import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PaginationInstance } from 'ngx-pagination';
import { Observable } from 'rxjs/Rx';
import { AuthService } from 'ng2-ui-auth';

import { ArticleModel } from '../../shared/models/article.model';
import { RestApi } from '../../shared/services/restapi.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  articles$: Observable<ArticleModel[]>;
  loading: boolean;
  currentUrl: string;
  queryParams: any;
  hasCreateArticlePermission: boolean;

  public config: PaginationInstance = {
    id: 'custom',
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(
    private auth: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private restApi: RestApi,
  ) {}

  ngOnInit() {
    this.queryParams = {};
    this.route.params
      .subscribe((params:Params) => {
        // console.log('list - route params', params);
        // console.log('list - current url', this.router.url);
        if (params.page) {
          this.config.currentPage = +params.page;
        } else {
          this.config.currentPage = 1;
        }
        if (params.tag) {
          this.queryParams['tag'] = params.tag;
        }
        this.currentUrl = this.router.url;
        this.loadPage(this.config.currentPage);
      });

    let payload = this.auth.getPayload();
    this.hasCreateArticlePermission = payload && payload.permissions && payload.permissions.indexOf('Create-Article') > -1;
  }

  onPageChange(page: number) {
    let baseUrl = this.queryParams['tag']
      ? '/articles/tag/'+this.queryParams['tag']
      : '/articles/list';
    this.router.navigate([baseUrl, page]);
  }

  loadPage(page: number) {
    this.loading = true;
    let url = this.applyFilters('articles', {page:page});

    this.articles$ = this.restApi.get(url)
      .map(res => res.json())
      .do(res => {
        this.config.currentPage = page;
        this.config.totalItems = res.total;
        this.loading = false;
      })
      .map(res => res.data);
  }

  applyFilters(url: string, filters?: any) {
    let params = {};

    for (let key in this.queryParams) {
      params[key] = this.queryParams[key];
    }
    // overwrite filters
    for (let key in filters) {
      params[key] = filters[key];
    }

    let aParams = [];
    for (let key in params) {
      aParams.push(`${key}=${params[key]}`);
    }
    let sParams = aParams.length ? `?${aParams.join('&')}` : '';
    return url + sParams;
  }

  onClearTagFilter() {
    this.router.navigate(['/articles']);
  }

}
