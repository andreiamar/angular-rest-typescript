import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-article-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {

  @Output("tags")
  data = new EventEmitter();

  @Input("tags")
  tags:string[] = []
  newTag:FormControl

  constructor() { }

  ngOnInit() {
    this.newTag = new FormControl();
  }

  add(input: HTMLInputElement): void {
    if (input.value && input.value.trim() !== '') {
      this.addTag(input.value.trim().toLowerCase());
      input.value = '';
    }
  }

  onRemoveTag(tag) {
    let index = this.tags.indexOf(tag);
    if (index > -1) {
      this.tags.splice(index, 1);
    }
  }

  addTag(name) {
    let exists = this.tags.find(tag => tag===name);
    if (!exists) {
      this.tags.push(name);
      this.data.emit(this.tags);
    }
    this.newTag.reset();
  }

}
