export const environment = {
  production: true,
  apiBaseUrl: 'https://cottus-io.herokuapp.com/api/',
  googleClientID: '492095763834-fg39c3co49trpjj8qpjjb50g90s5qvo8.apps.googleusercontent.com',
  facebookClientID: '238253776645295'
};
