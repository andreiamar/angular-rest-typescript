import {Sequelize, DataTypes} from 'sequelize';

export interface IPersona {
  user_id?: number,
  nickname?: string,
  photo?: string,
  is_default?: boolean
}

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let Persona:any = {};
  Persona = sequelize.define('Persona', {
    user_id: dataTypes.INTEGER,
    nickname: dataTypes.STRING,
    photo: dataTypes.STRING,
    is_default: dataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Persona.belongsTo(models.User, {foreignKey: 'user_id'});
      },
      add: function(values:IPersona) {
        Persona.create(values)
      }
    }
  });
  return Persona;
};
