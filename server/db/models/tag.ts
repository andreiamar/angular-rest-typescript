import {Sequelize, DataTypes, Transaction} from 'sequelize';
import db from './index';

export interface ITag {
  name: string,
  type?: string
}

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let Tag:any = {};
  Tag = sequelize.define('Tag', {
    name: dataTypes.STRING,
    type: dataTypes.STRING
  }, {
    classMethods: {
      associate: models => {
        // associations can be defined here
        Tag.hasMany(models.ArticleTagMapping, {as:'tag',foreignKey:'tag_id'});
      },
      add: function(tag:ITag) {
        return Tag.create(tag);
      },
      delete: function(id:number) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            db.ArticleTagMapping.destroy({
              where: { tag_id: id },
              transaction: t
            })
              .then((numDeleted) => {
                return Tag.destroy({
                  where: {
                    id: id
                  },
                  transaction: t
                });
              })
              .then(numDeleted => resolve())
              .catch(reject);
          });
        });
      },
      getTagIds: function(tags:string[], t:Transaction) {
        let promises = tags.map(tagName => {
          let where = {name: tagName.toLowerCase()};
          let opts = {where: where, transaction: t};
          return Tag.findOrCreate(opts);
        });
        return Promise.all(promises);
      }
    },
    instanceMethods: {
    }
  });
  return Tag;
};
