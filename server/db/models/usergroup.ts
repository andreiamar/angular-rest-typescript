import {Sequelize, DataTypes, Transaction} from 'sequelize';
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let UserGroup:any = {};
  UserGroup = sequelize.define('UserGroup', {
    name: dataTypes.STRING,
    category: dataTypes.STRING,
    is_default: dataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        UserGroup.hasMany(models.UserGroupMapping, {as:'userGroups',foreignKey:'user_group_id'});
        UserGroup.hasMany(models.UserGroupPermissionMapping, {as:'permissionGroupMappings',foreignKey:'user_group_id'});
      },

      add: function(params) {
        return new Promise((resolve, reject) => {
          UserGroup.findOne({where: {name: params.name}})
            .then(exists => {
              if (exists) {
                return resolve(exists);
              }
              UserGroup.create(params)
                .then(result => resolve(result));
            })
        });
      },

      put: function(id:number, name:string) {
        return new Promise((resolve, reject) => {
          UserGroup.findById(id)
            .then(exists => {
              if (!exists) {
                return reject(HTTPStatusCodes.NOT_FOUND);
              }
              const fields = {name: name};
              const opts = {where: {id: exists.id}};
              UserGroup.update(fields, opts)
                .then(result => resolve(result))
                .catch(reject);
            })
        });
      },

      delete: function(id:number) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            // remove assigned tags
            sequelize.models.PermissionGroupMapping.destroy({
              where: { permission_id: id }
            })
            .then(() => {
              return UserGroup.destroy({
                where: { 'id': id }
              });
            })
            .then(resolve)
            .catch(reject);
          });
        });
      }
    }
  });
  return UserGroup;
};
