import {Sequelize, DataTypes} from 'sequelize';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let UserbaseScope = sequelize.define('UserbaseScope', {
    name: dataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return UserbaseScope;
};
