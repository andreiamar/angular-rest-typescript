import {Sequelize, DataTypes, Transaction} from 'sequelize';
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let PermissionGroup:any = {};
  PermissionGroup = sequelize.define('PermissionGroup', {
    name: dataTypes.STRING,
    rank: dataTypes.INTEGER,
    group_type: dataTypes.STRING,
    is_default: dataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        PermissionGroup.hasMany(models.PermissionGroupMapping, {as:'permissionGroups',foreignKey:'permission_group_id'});
        PermissionGroup.hasMany(models.UserGroupPermissionMapping, {as:'permissionGroup',foreignKey:'permission_group_id'});
      },

      add: function(params) {
        return new Promise((resolve, reject) => {
          PermissionGroup.findOne({where: {name: params.name}})
            .then(exists => {
              if (exists) {
                return resolve(exists);
              }
              PermissionGroup.create(params)
                .then(result => resolve(result));
            })
        });
      },

      put: function(id:number, name:string) {
        return new Promise((resolve, reject) => {
          PermissionGroup.findById(id)
            .then(exists => {
              if (!exists) {
                return reject(HTTPStatusCodes.NOT_FOUND);
              }
              const fields = {name: name};
              const opts = {where: {id: exists.id}};
              PermissionGroup.update(fields, opts)
                .then(result => resolve(result))
                .catch(reject);
            })
        });
      },

      delete: function(id:number) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            PermissionGroup.destroy({
              where: { 'id': id }
            })
            .then(resolve)
            .catch(reject);
          });
        });
      }
    }
  });
  return PermissionGroup;
};
