import {Sequelize, DataTypes, Transaction} from 'sequelize';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let UserGroupPermissionMapping:any = {};
  UserGroupPermissionMapping = sequelize.define('UserGroupPermissionMapping', {
    user_group_id: dataTypes.INTEGER,
    permission_group_id: dataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        UserGroupPermissionMapping.belongsTo(models.UserGroup, {as:'permissionGroupMappings', foreignKey:'user_group_id'});
        UserGroupPermissionMapping.belongsTo(models.PermissionGroup, {as:'permissionGroup',foreignKey:'permission_group_id'});
      },

      updateUserGroupPermissions: function(groupId:number, permissionGroups:number[]) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            UserGroupPermissionMapping.destroy({
              where: { user_group_id: groupId }
            })
            .then(() => UserGroupPermissionMapping.assignUserGroupPermissions(groupId, permissionGroups, t))
            .then(resolve)
            .catch(reject);
          });
        });
      },

      assignUserGroupPermissions: function(groupId:number, permissionGroups:number[], t:Transaction) {
        let promises = permissionGroups.map(permissionGroupId => {
          const assoc = {
            user_group_id: groupId,
            permission_group_id: permissionGroupId
          };
          return UserGroupPermissionMapping.create(assoc, {transaction: t});
        });
        return Promise.all(promises);
      }
    }
  });
  return UserGroupPermissionMapping;
};
