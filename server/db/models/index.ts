import * as fs from 'fs';
import * as path from 'path';
import * as ORM from 'sequelize';
import { SeedDefaults } from '../seed/defaults';
import { Sequelize, LoggingOptions } from 'sequelize';
const basename  = path.basename(module.filename);
const env       = process.env.NODE_ENV || 'development';
const doDbSync  = (process.env.SYNC_DB==='true');

let db:any = {};
let sequelize;

if (process.env["DATABASE_URL"]) {
  console.log('DATABASE_URL', process.env["DATABASE_URL"]);
  sequelize = new ORM(process.env["DATABASE_URL"]);
} else {
  const config = {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT || 'postgres'
  };
  console.log('DB CONFIG', config);
  sequelize = new ORM(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.ts');
  })
  .forEach(function(file) {
    let model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Db connection successful');
    console.log('Syncing db...');
    return sequelize.sync({
      force: doDbSync
    })
  })
  .then(() => {
    if (doDbSync) {
      return SeedDefaults.init(db);
    }
    return Promise.resolve();
  })
  .then(function () {
    console.log("Database synchronized!");
    console.log("Ready.");
  }).catch(function (err) {
    console.error("An error occurred: ", err);
  });

db['sequelize'] = sequelize;
db['Sequelize'] = ORM;

export default db;
