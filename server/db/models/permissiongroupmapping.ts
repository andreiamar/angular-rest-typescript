import {Sequelize, DataTypes, Transaction} from 'sequelize';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let PermissionGroupMapping:any = {};
  PermissionGroupMapping = sequelize.define('PermissionGroupMapping', {
    permission_group_id: dataTypes.INTEGER,
    permission_id: dataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        PermissionGroupMapping.belongsTo(models.Permission, {as:'permissionGroupMappings', foreignKey:'permission_id'});
        PermissionGroupMapping.belongsTo(models.PermissionGroup, {as:'permissionGroups',foreignKey:'permission_group_id'});
      },

      updateGroupPermissions: function(groupId:number, permissions:number[]) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            PermissionGroupMapping.destroy({
              where: { permission_group_id: groupId }
            })
            .then(() => PermissionGroupMapping.assignGroupPermissions(groupId, permissions, t))
            .then(resolve)
            .catch(reject);
          });
        });
      },

      assignGroupPermissions: function(groupId:number, permissions:number[], t:Transaction) {
        let promises = permissions.map(permissionId => {
          const assoc = {
            permission_group_id: groupId,
            permission_id: permissionId
          };
          return PermissionGroupMapping.create(assoc, {transaction: t});
        });
        return Promise.all(promises);
      }
    }
  });
  return PermissionGroupMapping;
};
