import {Sequelize, DataTypes, Transaction} from 'sequelize';
import db from './index';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let ArticleTagMapping:any = {};
  ArticleTagMapping = sequelize.define('ArticleTagMapping', {
    article_id: dataTypes.INTEGER,
    tag_id: dataTypes.INTEGER
  }, {
    classMethods: {
      associate: models => {
        // associations can be defined here
        ArticleTagMapping.belongsTo(models.Article, {as:'tags',foreignKey:'article_id'});
        ArticleTagMapping.belongsTo(models.Tag, {as:'tag',foreignKey:'tag_id'});
      },

      assignTags: function(articleId:number, tags:string[], t: Transaction) {
        return new Promise((resolve, reject) => {
          let opts = {
            where: {article_id:articleId},
            transaction: t
          };
          // 0. delete existing associations
          ArticleTagMapping.destroy(opts)
            // 1. create tags that do not exist
            .then(() => db.Tag.getTagIds(tags, t))
            // 2. assign tag ids to article
            .then((res) => ArticleTagMapping.saveTags(articleId, res, t))
            .then(() => resolve())
            .catch(reject);
        });
      },

      saveTags: function(articleId:number, promiseResults:any[], t:Transaction) {
        let promises = promiseResults.map(tagsCreated => {
          const assoc = {
            article_id: articleId,
            tag_id: tagsCreated[0].id
          };
          return ArticleTagMapping.create(assoc, {transaction: t});
        });
        return Promise.all(promises);
      }
    },

    instanceMethods: {
    }
  });
  return ArticleTagMapping;
};
