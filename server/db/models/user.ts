import {Sequelize, DataTypes, Transaction} from 'sequelize';
import * as jwt from 'jwt-simple';
import * as moment from 'moment';
import * as bcrypt from 'bcrypt-nodejs';
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';
import db from './index';
import { IPersona } from './persona';

export interface IUser {
  email: string,
  firstname: string,
  lastname: string,
  password?: string,
  google?: string,
  google_profile?: string,
  facebook?: string,
  facebook_profile?: string,
}

export interface IGoogleUser {
  sub: string,
  name: string,
  given_name: string,
  family_name: string,
  profile: string,
  picture: string,
  email: string,
  locale: string,
}
export interface IFacebookUser {
  id: string,
  email: string,
  first_name: string,
  last_name: string,
  link: string,
  name: string,
  picture?: string,
}

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let User:any = {};
  User = sequelize.define('User', {
    email: dataTypes.STRING,
    firstname: dataTypes.STRING,
    lastname: dataTypes.STRING,
    password: dataTypes.STRING,
    google: dataTypes.STRING,
    google_profile: dataTypes.TEXT,
    facebook: dataTypes.STRING,
    facebook_profile: dataTypes.TEXT,
    user_type: dataTypes.STRING,
    pref_language_id: dataTypes.INTEGER
  }, {
    classMethods: {
      associate: models => {
        // associations can be defined here
        User.hasMany(models.Persona, {foreignKey: 'user_id'});
        User.hasOne(models.Article, {foreignKey: 'user_id'});
        User.hasMany(models.UserGroupMapping, {as:'userGroups',foreignKey:'user_id'});
      },

      get: function(id:number) {
        return User.findById(id, {
          include: [{
            model: db.Persona,
            where: { is_default: true }
          }]
        })
      },

      put: function(id:number, fields:any) {
        if (fields.password) {
          fields.password = User.hashPassword(fields.password);
        }
        return User.update(fields, { where: { id: id } });
      },

      delete: function(userId:number) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            // remove assigned groups
            db.UserGroupMapping.destroy({where:{user_id:userId}})
            .then(() => db.Persona.destroy({where:{user_id:userId}}))
            .then(() => User.destroy({where: {id: userId}}))
            .then(resolve)
            .catch(reject)
          });
        });
      },

      findByGoogle: function(googleId:string) {
        return User.findOne({where: { google: googleId }});
      },

      findByFacebook: function(facebookId:string) {
        return User.findOne({where: { facebook: facebookId }});
      },

      /**
      Creates a new user with a default persona
      */
      add: function(user:IUser, persona:IPersona) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            // test that the email address is unique
            User.findOne({where:{email:user.email}})
            .then(foundUser => {
              if (foundUser) {
                return reject({
                  statusCode: HTTPStatusCodes.CONFLICT,
                  message: "Email is already used"
                });
              }
              if (user.password) {
                user.password = User.hashPassword(user.password);
              }
              return User.create(user, {transaction:t})
              .then(newUser => {
                // create the default user Persona
                if (!persona) {
                  persona = {
                    nickname: `${newUser.firstname} ${newUser.lastname}`,
                  };
                }
                persona.is_default = true;
                persona.user_id = newUser.id;
                return db.Persona.create(persona, {transaction:t})
                  .then(() => this.assignDefaultUserGroup(newUser.id, t))
                  .then(() => {
                    resolve(newUser);
                  });
              });
            })
            .catch(reject);
          });
        });
      },

      assignDefaultUserGroup: function(userId, t:Transaction) {
        return db.UserGroup.findOne({where:{is_default:true}})
          .then(group => {
            if (!group) {
              return Promise.reject(new Error('Default user group not found'));
            }
            return db.UserGroupMapping.assignGroupUsers(group.id, [userId], t);
          })
      },

      /**
      Create a new user account or return an existing one.
      */
      addGoogle: function(profile:IGoogleUser) {
        return new Promise((resolve, reject) => {
          User.findOne({
            include: [{
              model: db.Persona,
              where: { is_default: true }
            }],
            where: { google: profile.sub }
          })
            .then(existingUser => {
              if (existingUser) {
                return resolve(existingUser);
              }
              // test if a user with profile.email exists
              return User.findOne({where:{email:profile.email}});
            })
            .then(foundUser => {
              if (foundUser) {
                // link the google account
                return foundUser.update({
                  google: profile.sub,
                  google_profile: JSON.stringify(profile)
                });
              } else {
                // user not found; create a new user
                console.log('adding new user');
                let user:IUser = {
                  email: profile.email,
                  firstname: profile.given_name,
                  lastname: profile.family_name,
                  google: profile.sub,
                  google_profile: JSON.stringify(profile),
                }
                let persona:IPersona = {
                  nickname: `${user.firstname} ${user.lastname}`,
                  photo: profile.picture.replace('sz=50', 'sz=200'),
                }
                return User.add(user, persona);
              }
            })
            .then(user => resolve(user))
            .catch(reject);
        });
      },

      /**
      Create a new user account or return an existing one.
      */
      addFacebook: function(profile:IFacebookUser) {
        return new Promise((resolve, reject) => {
          User.findOne({
            include: [{
              model: db.Persona,
              where: { is_default: true }
            }],
            where: { facebook: profile.id }
          })
            .then(existingUser => {
              if (existingUser) {
                return resolve(existingUser);
              }
              // test if a user with profile.email exists
              return User.findOne({where:{email:profile.email}});
            })
            .then(foundUser => {
              if (foundUser) {
                // link the facebook account
                return foundUser.update({
                  facebook: profile.id,
                  facebook_profile: JSON.stringify(profile)
                });
              } else {
                // user not found; create a new user
                console.log('adding new user');
                let user:IUser = {
                  email: profile.email,
                  firstname: profile.first_name,
                  lastname: profile.last_name,
                  facebook: profile.id,
                  facebook_profile: JSON.stringify(profile),
                }
                let persona:IPersona = {
                  nickname: `${user.firstname} ${user.lastname}`,
                  photo: `https://graph.facebook.com/v2.3/${profile.id}/picture?type=large`,
                }
                return User.add(user, persona);
              }
            })
            .then(user => resolve(user))
            .catch(reject);
        });
      },

      /**
      Link Google account
      */
      linkGoogle: function(token:string, profile:IGoogleUser) {
        let payload = jwt.decode(token, process.env.SECRET);

        return new Promise((resolve, reject) => {
          User.get(payload.sub)
            .then(user => {
              if (!user) {
                return reject({
                  statusCode: HTTPStatusCodes.BAD_REQUEST,
                  message: 'User not found'
                });
              }
              user.update({
                google: profile.sub,
                google_profile: JSON.stringify(profile)
              })
              .then(updatedUser => {
                // set the default persona's photo
                let persona = (user.Personas) ? user.Personas[0] : null;
                if (persona && !persona.photo) {
                  return persona.update({
                    photo: profile.picture.replace('sz=50', 'sz=200')
                  })
                  .then((updatedPersona) => { resolve(updatedUser) })
                  .catch(reject);
                }
                // else
                resolve(updatedUser);
              })
              .catch(reject);
              //user.picture = user.picture || profile.picture.replace('sz=50', 'sz=200');
            });
        });
      },

      /**
      Link Facebook account
      */
      linkFacebook: function(token:string, profile:IFacebookUser) {
        let payload = jwt.decode(token, process.env.SECRET);

        return new Promise((resolve, reject) => {
          User.get(payload.sub)
            .then(user => {
              if (!user) {
                return reject({
                  statusCode: HTTPStatusCodes.BAD_REQUEST,
                  message: 'User not found'
                });
              }
              user.update({
                facebook: profile.id,
                facebook_profile: JSON.stringify(profile)
              })
              .then(updatedUser => {
                // set the default persona's photo
                let persona = (user.Personas) ? user.Personas[0] : null;
                if (persona && !persona.photo) {
                  return persona.update({
                    photo: `https://graph.facebook.com/v2.3/${profile.id}/picture?type=large`
                  })
                  .then((updatedPersona) => { resolve(updatedUser) })
                  .catch(reject);
                }
                // else
                resolve(updatedUser);
              })
              .catch(reject);
              //user.picture = user.picture || profile.picture.replace('sz=50', 'sz=200');
            });
        });
      },

      hashPassword: function(password) {
        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
      },

      getPermissions: function(userId) {
        return new Promise((resolve, reject) => {
          // find user permissions
          db.UserGroupMapping.findAll({
            where: { user_id: userId },
            include: [{
              model: db.UserGroup,
              as: 'userGroup',
              include: [{
                model: db.UserGroupPermissionMapping,
                as: 'permissionGroupMappings'
              }]
            }]
          }).then(results => {
            let permissionGroups = [];
            results.forEach(userGroupMapping => {
              if (userGroupMapping.userGroup.permissionGroupMappings) {
                userGroupMapping.userGroup.permissionGroupMappings.forEach(item => {
                  permissionGroups.push(item.permission_group_id);
                })
              }
            });

            return db.Permission.findAll({
              include: [{
                model: db.PermissionGroupMapping,
                as: 'permissionGroupMappings',
                where: {
                  permission_group_id: {
                    $in: permissionGroups
                  }
                }
              }]
            }).then(results => {
              let permissions = [];
              if (results && results.length) {
                permissions = results.map(item => item.name);
              }
              resolve(permissions);
            }).catch(reject)
          }).catch(reject)
        })
      },
    },
    instanceMethods: {
      passwordMatches: function(password) {
        return bcrypt.compareSync(password, this.password);
      },
      createJWT: function() {
        return new Promise((resolve, reject) => {

          this.getPermissions().then(permissions => {
            let payload = {
              iat: moment().unix(),
              exp: moment().add(14, 'days').unix(),
              sub: this.id,
              firstname: this.firstname,
              lastname: this.lastname,
              email: this.email,
              google: this.google,
              google_profile: this.google_profile,
              facebook: this.facebook,
              facebook_profile: this.facebook_profile,
            };
            if (permissions && permissions.length) {
              payload['permissions'] = permissions;
            }

            if (this.Personas) {
              let persona = this.Personas[0];
              payload['persona_nickname'] = persona.nickname;
              payload['persona_photo'] = persona.photo;
            }

            let token = jwt.encode(payload, process.env.SECRET);
            resolve(token);
          }).catch(reject)
        });
      },
      getPermissions: function() {
        return User.getPermissions(this.id);
      }
    }
  });
  return User;
};
