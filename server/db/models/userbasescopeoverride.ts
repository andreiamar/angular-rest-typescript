import {Sequelize, DataTypes} from 'sequelize';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let UserbaseScopeOverride = sequelize.define('UserbaseScopeOverride', {
    userbase_scope_id: dataTypes.INTEGER,
    user_group_id: dataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return UserbaseScopeOverride;
};
