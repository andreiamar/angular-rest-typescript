import {Sequelize, DataTypes, Transaction} from 'sequelize';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let UserGroupMapping:any = {};
  UserGroupMapping = sequelize.define('UserGroupMapping', {
    user_id: dataTypes.INTEGER,
    user_group_id: dataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        UserGroupMapping.belongsTo(models.User, {as:'users', foreignKey:'user_id'});
        UserGroupMapping.belongsTo(models.UserGroup, {as:'userGroup',foreignKey:'user_group_id'});
      },

      updateGroupUsers: function(groupId:number, users:number[]) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            UserGroupMapping.destroy({
              where: { user_group_id: groupId }
            })
            .then(() => UserGroupMapping.assignGroupUsers(groupId, users, t))
            .then(resolve)
            .catch(reject);
          });
        });
      },

      assignGroupUsers: function(groupId:number, users:number[], t:Transaction) {
        let promises = users.map(userId => {
          const assoc = {
            user_group_id: groupId,
            user_id: userId
          };
          return UserGroupMapping.create(assoc, {transaction: t});
        });
        return Promise.all(promises);
      }
    }
  });
  return UserGroupMapping;
};
