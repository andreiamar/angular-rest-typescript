import {Sequelize, DataTypes, Transaction} from 'sequelize';
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let Permission:any = {};
  Permission = sequelize.define('Permission', {
    name: dataTypes.STRING,
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Permission.hasMany(models.PermissionGroupMapping, {as:'permissionGroupMappings',foreignKey:'permission_id'});
      },

      add: function(params) {
        return new Promise((resolve, reject) => {
          Permission.findOne({where: {name: params.name}})
            .then(exists => {
              if (exists) {
                return resolve(exists);
              }
              Permission.create(params)
                .then(result => resolve(result));
            })
        });
      },

      put: function(id:number, name:string) {
        return new Promise((resolve, reject) => {
          Permission.findById(id)
            .then(exists => {
              if (!exists) {
                return reject(HTTPStatusCodes.NOT_FOUND);
              }
              const fields = {name: name};
              const opts = {where: {id: exists.id}};
              Permission.update(fields, opts)
                .then(result => resolve(result))
                .catch(reject);
            })
        });
      },

      delete: function(id:number) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            // remove assigned tags
            sequelize.models.PermissionGroupMapping.destroy({
              where: { permission_id: id }
            })
            .then(() => {
              return Permission.destroy({
                where: { 'id': id }
              });
            })
            .then(resolve)
            .catch(reject);
          });
        });
      }
    }
  });
  return Permission;
};
