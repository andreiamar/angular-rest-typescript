import {Sequelize, DataTypes, Transaction} from 'sequelize';
import db from './index';
import { ITag } from './tag';
import * as slug from 'slug';

export interface IArticle {
  user_id: number,
  title: string,
  slug?: string,
  body: string,
  language_id?: number
}

export default function(sequelize: Sequelize, dataTypes: DataTypes) {
  let Article:any = {};
  Article = sequelize.define('Article', {
    user_id: dataTypes.INTEGER,
    title: dataTypes.STRING,
    slug: dataTypes.STRING,
    body: dataTypes.TEXT,
    copyright: dataTypes.STRING,
    publication_status: dataTypes.STRING,
    language_id: dataTypes.STRING
  }, {
    classMethods: {
      associate: models => {
        // associations can be defined here
        Article.hasMany(models.ArticleTagMapping, {as:'tags',foreignKey:'article_id'});
        Article.belongsTo(models.User, {as:'author',foreignKey:'user_id'})
      },
      get: function(articleId) {
        const opts = {
          include: [
            {
              model: db.ArticleTagMapping,
              as: 'tags',
              include: [
                {
                  model: db.Tag,
                  as: 'tag'
                }
              ]
            },
            {
              model: db.User,
              as: 'author'
            }
          ]
        };
        return db.Article.findById(articleId, opts);
      },
      add: function(article:IArticle, tags:string[]) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            let savedResult = null;
            article.slug = slug(article.title).toLowerCase();
            Article.create(article, {transaction: t})
              .then(newArticle => {
                savedResult = newArticle;
                return db.ArticleTagMapping.assignTags(newArticle.id, tags, t);
              })
              .then(() => resolve(savedResult))
              .catch(reject);
          });
        });
      },
      put: function(articleId:number, article:IArticle, tags:string[]) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            let savedResult = null;
            // don't need to update the user_id
            delete article.user_id;
            article.slug = slug(article.title);
            const opts = {
              transaction: t,
              where: {id: articleId}
            };
            Article.update(article, opts)
              .then(res => {
                return db.ArticleTagMapping.assignTags(articleId, tags, t);
              })
              .then(resolve)
              .catch(reject);
          });
        });
      },
      delete: function(articleId:number) {
        return sequelize.transaction((t: Transaction) => {
          return new Promise((resolve, reject) => {
            // remove assigned tags
            db.ArticleTagMapping.destroy({
              where: { article_id: articleId }
            })
            .then(() => {
              return Article.destroy({
                where: { 'id': articleId }
              });
            })
            .then(resolve)
            .catch(reject);
          });
        });
      }
    },
    instanceMethods: {
    }
  });
  return Article;
};
