
export class SeedDefaults {

  static defaultPermissions = [
    'Create-Article',
    'Read-Own-Article',
    'Read-Public-Article',
    'Edit-Own-Article',
    'Edit-All-Articles',
    'Delete-Own-Article',
    'Delete-All-Articles'
  ];

  static adminPermissions = [
    'Admin-Functions'
  ];

  static adminGroupName = 'Administrators';

  static init(db:any) {
    return new Promise((resolve, reject) => {
      this.ensureDefaultPermissionGroupExists(db)
        .then((defaultPermissionGroup) => this.ensureDefaultUserGroupExists(db, defaultPermissionGroup))
        .then(() => this.ensureAdminPermissionGroupExists(db))
        .then((adminPermissionGroup) => this.ensureAdminGroupExists(db, adminPermissionGroup))
        .then((adminUserGroup) => this.ensureAdminUserExists(db, adminUserGroup))
        .then(() => resolve)
        .catch(reject)
    });
  }

  static ensureDefaultPermissionGroupExists(db:any) {
    return new Promise((resolve, reject) => {
      let data:any = {};
      db.PermissionGroup.findOne({where:{is_default:true}})
        .then(result => {
          if (result) {
            return resolve(result);
          }
          return db.PermissionGroup.create({
            name: 'Default',
            is_default: true
          }).then(result => {
            data.group = result;
            return this.createDefaultPermissions(db)
          }).then((permissions) => {
            let permissionIds = permissions.map(permission => permission.id);
            return db.PermissionGroupMapping.assignGroupPermissions(data.group.id, permissionIds);
          }).then(() => resolve(data.group));
        })
        .catch(reject);
    });
  }

  static ensureAdminPermissionGroupExists(db:any) {
    return new Promise((resolve, reject) => {
      let data:any = {};
      db.PermissionGroup.findOne({where:{name:this.adminGroupName}})
        .then(result => {
          if (result) {
            return resolve(result);
          }
          return db.PermissionGroup.create({
            name: this.adminGroupName,
            is_default: false
          }).then(result => {
            data.group = result;
            return this.createAdminPermissions(db)
          }).then((permissions) => {
            let permissionIds = permissions.map(permission => permission.id);
            return db.PermissionGroupMapping.assignGroupPermissions(data.group.id, permissionIds);
          }).then(() => resolve(data.group));
        })
        .catch(reject);
    });
  }

  static createDefaultPermissions(db) {
    let promises = this.defaultPermissions.map(permissionName => {
      return db.Permission.add({name:permissionName});
    });
    return Promise.all(promises);
  }

  static ensureDefaultUserGroupExists(db:any, defaultPermissionGroup:any) {
    return new Promise((resolve, reject) => {
      let data:any = {};
      db.UserGroup.findOne({where:{is_default:true}})
        .then(result => {
          if (result) {
            return resolve(result);
          }
          return db.UserGroup.create({
            name: 'Default',
            is_default: true
          }).then(result => {
            data.group = result;
            return db.UserGroupPermissionMapping.assignUserGroupPermissions(data.group.id, [defaultPermissionGroup.id]);
          }).then(() => resolve());
        })
        .catch(reject);
    });
  }

  static ensureAdminGroupExists(db:any, adminPermissionGroup:any) {
    return new Promise((resolve, reject) => {
      let data:any = {};
      db.UserGroup.findOne({where:{name:this.adminGroupName}})
        .then(result => {
          if (result) {
            return resolve(result);
          }
          return db.UserGroup.create({
            name: this.adminGroupName,
            is_default: false
          }).then(result => {
            data.group = result;
            return db.UserGroupPermissionMapping.assignUserGroupPermissions(data.group.id, [adminPermissionGroup.id]);
          }).then(() => resolve(data.group));
        })
        .catch(reject);
    });
  }

  static createAdminPermissions(db) {
    let promises = this.adminPermissions.map(permissionName => {
      return db.Permission.add({name:permissionName});
    });
    return Promise.all(promises);
  }

  static ensureAdminUserExists(db, adminUserGroup) {
    return db.User.add({
      email: process.env.ADMIN_EMAIL,
      password: process.env.ADMIN_PASSWORD,
      firstname: process.env.ADMIN_FIRSTNAME,
      lastname: process.env.ADMIN_LASTNAME
    }).then(user => {
      return db.UserGroupMapping.assignGroupUsers(adminUserGroup.id, [user.id]);
    });
  }
}
