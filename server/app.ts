import * as express from 'express';
import {Application} from 'express';
import * as path from 'path';
import * as favicon from 'serve-favicon';
import * as logger from 'morgan';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as bearerToken from 'express-bearer-token';
import * as cors from 'cors';
// ensure that the env vars are set before loading anything else
import requireEnvVars from './middlewares/dotenv.middleware';
requireEnvVars({path: path.resolve(__dirname, '../.env')});

import { winston } from './lib/logger/index';

const app: Application = express();

app.use(function (req, res, next) {
  // console.log('headers', JSON.stringify(req.headers, null, 2));
  // console.log('secret', process.env.SECRET);
  // console.log('db url', process.env.DATABASE_URL);
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bearerToken());
app.use(cors());

app.use(express.static(path.join(__dirname, '../client/dist')));

// connect routes
import routes from './routes';
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err['status'] = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  console.error(err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  if (res.headersSent) {
    return next(err);
  }

  if (req.headers['content-type'] && req.headers['content-type']==='application/json') {
    return res.status(err.status || 500).json({
      error: res.locals.error ? res.locals.error.message : ''
    });
  }
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;
