import * as path from 'path';
import * as express from 'express';
const router: express.Router = express.Router();
import apiRoute from './api';

router.use('/api', apiRoute);

router.use(function(req, res, next) {
  // serve the client angular app in production only
  console.log('Serving client index.html ...');
  if (req.app.get('env')==='production') {
    res.sendFile(path.join(__dirname, '../../client/dist/index.html'));
  }
});

export default router;
