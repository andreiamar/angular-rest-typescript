import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import ProfileController from '../../controllers/profile';

router.put('/me', ProfileController.update);

export default router;
