import * as express from 'express';
const router: express.Router = express.Router();

import { ensureAuthenticated } from '../../middlewares/auth.middleware';
import { loadUserPermissions, hasPermissions, hasAdminPermissions } from '../../middlewares/permission.middleware';

import articleRoutes from './articles';
import tagRoutes from './tags';
import profileRoutes from './profile';
import permissionsRoutes from './permissions';
import permissionGroupsRoutes from './permissionGroups';
import groupPermissionsRoutes from './groupPermissions';
import userGroupsRoutes from './userGroups';
import userGroupPermissionGroupsRoutes from './userGroupPermissionGroups';
import userGroupUsersRoutes from './userGroupUsers';
import usersRoutes from './users';

import AuthController from '../../controllers/auth';
import ProfileController from '../../controllers/profile';
import ArticlesController from '../../controllers/articles';

// public routes
router.get('/auth/refresh', AuthController.refresh);
router.post('/auth/login',  AuthController.login);
router.post('/auth/signup', AuthController.signup);
router.post('/auth/google', AuthController.google);
router.post('/auth/facebook', AuthController.facebook);

// public articles routes
router.get('/articles/', ArticlesController.list);
router.get('/articles/:id', ArticlesController.get);

// Authorize the request;
// All subsequent routes should rely on req['user'] being set
router.use(ensureAuthenticated);

// Load permissions for current user
router.use(loadUserPermissions);

router.use('/profile', profileRoutes);

router.use('/articles', articleRoutes);
router.use('/tags', tagRoutes);

// admin - permissions
router.use('/permissions', hasAdminPermissions(), permissionsRoutes);
router.use('/permission-groups', hasAdminPermissions(), permissionGroupsRoutes);
router.use('/group-permissions', hasAdminPermissions(), groupPermissionsRoutes);
router.use('/user-groups', hasAdminPermissions(), userGroupsRoutes);
router.use('/usergroup-permissiongroups', hasAdminPermissions(), userGroupPermissionGroupsRoutes);
router.use('/usergroup-users', hasAdminPermissions(), userGroupUsersRoutes);
router.use('/users', hasAdminPermissions(), usersRoutes);

export default router;
