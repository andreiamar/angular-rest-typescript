import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import UserGroupPermissionGroupsController from '../../controllers/usergroupPermissionGroups';

router.get('/:id', UserGroupPermissionGroupsController.get);
router.put('/:id', UserGroupPermissionGroupsController.update);

export default router;
