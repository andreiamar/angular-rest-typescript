import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import UsersController from '../../controllers/users';

router.get('/', UsersController.list);
router.get('/:id', UsersController.get);
router.put('/:id', UsersController.update);
router.delete('/:id', UsersController.delete);

export default router;
