import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import PermissionsController from '../../controllers/permissions';

router.get('/', PermissionsController.list);
router.get('/:id', PermissionsController.get);
router.put('/:id', PermissionsController.update);
router.post('/', PermissionsController.create);
router.delete('/:id', PermissionsController.delete);

export default router;
