import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import UserGroupsController from '../../controllers/userGroups';

router.get('/', UserGroupsController.list);
router.get('/:id', UserGroupsController.get);
router.put('/:id', UserGroupsController.update);
router.post('/', UserGroupsController.create);
router.delete('/:id', UserGroupsController.delete);

export default router;
