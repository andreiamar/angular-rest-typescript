import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import GroupPermissionsController from '../../controllers/groupPermissions';

router.get('/:id', GroupPermissionsController.get);
router.put('/:id', GroupPermissionsController.update);

export default router;
