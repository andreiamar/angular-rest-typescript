import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import PermissionGroupsController from '../../controllers/permissionGroups';

router.get('/', PermissionGroupsController.list);
router.get('/:id', PermissionGroupsController.get);
router.put('/:id', PermissionGroupsController.update);
router.post('/', PermissionGroupsController.create);
router.delete('/:id', PermissionGroupsController.delete);

export default router;
