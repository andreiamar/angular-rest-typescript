import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import UserGroupUsersController from '../../controllers/usergroupUsers';

router.get('/:id', UserGroupUsersController.get);
router.put('/:id', UserGroupUsersController.update);

export default router;
