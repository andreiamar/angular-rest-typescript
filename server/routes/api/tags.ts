import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';

import TagsController from '../../controllers/tags';

router.get('/', TagsController.list);
router.get('/:id', TagsController.get);
router.put('/:id', TagsController.update);
router.post('/', TagsController.create);
router.delete('/:id', TagsController.delete);

export default router;
