import * as express from 'express';
const router: express.Router = express.Router();
import { HTTPStatusCodes } from '../../lib/httpStatusCodes';
import { hasPermissions } from '../../middlewares/permission.middleware';

import ArticlesController from '../../controllers/articles';

// router.get('/', ArticlesController.list);
// router.get('/:id', ArticlesController.get);
router.post('/', ArticlesController.create);
router.put('/:id', ArticlesController.update);
router.delete('/:id', ArticlesController.delete);

export default router;
