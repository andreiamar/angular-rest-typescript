import { Request, Response } from 'express';
import * as moment from 'moment';
import * as jwt from 'jwt-simple';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';

/*
 |--------------------------------------------------------------------------
 | Login Required Middleware
 |--------------------------------------------------------------------------
 */
export function ensureAuthenticated(req: Request, res: Response, next) {
  if (!req.header('Authorization')) {
    return res.status(HTTPStatusCodes.UNAUTHORIZED).send({ message: 'Please make sure your request has an Authorization header' });
  }
  let token = req.header('Authorization').split(' ')[1];

  let payload = null;
  try {
    payload = jwt.decode(token, process.env.SECRET);
  }
  catch (err) {
    return res.status(HTTPStatusCodes.UNAUTHORIZED).send({ message: err.message });
  }

  if (payload.exp <= moment().unix()) {
    return res.status(HTTPStatusCodes.UNAUTHORIZED).send({ message: 'Token has expired' });
  }
  req['user'] = payload.sub;
  next();
}
