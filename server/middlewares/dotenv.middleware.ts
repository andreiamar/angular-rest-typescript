import * as dotenv from 'dotenv';
import * as fs from 'fs';

const env = process.env.NODE_ENV || 'development';
console.log('Node environment:', env);

function difference(arrA, arrB) {
    return arrA.filter(function (a) {
        return arrB.indexOf(a) < 0;
    });
}

function compact(obj) {
    var result = {};
    Object.keys(obj).forEach(function (key) {
        if (obj[key]) {
            result[key] = obj[key];
        }
    });
    return result;
}

export default function (options) {

    options = options || {};
    let result = { missing: [] };
    if (env !== 'development') {
      return;
    }
    // load env vars from .env
    dotenv.load(options);
    let sample = options.sample || '.env.example';
    let sampleVars = dotenv.parse(fs.readFileSync(sample));
    // remove non-empty values
    let processEnv = compact(process.env);
    let missing = difference(Object.keys(sampleVars), Object.keys(processEnv));
    if (missing.length > 0) {
      throw new Error('Env vars missing ==> '+missing.join(','));
    }
}
