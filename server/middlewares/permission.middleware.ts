import { Request, Response } from 'express';
import * as moment from 'moment';
import * as jwt from 'jwt-simple';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import db from '../db/models';

export function loadUserPermissions(req:Request, res:Response, next) {
  if (!req['user']) {
    return res.status(HTTPStatusCodes.UNAUTHORIZED);
  }
  db.User.getPermissions(req['user'])
    .then(permissions => {
      req['permissions'] = permissions;
      next();
    })
    .catch(err => {
      return res.status(HTTPStatusCodes.UNAUTHORIZED).send({message: err.message});
    })
}

export function hasPermissions(permissions) {
  return (req:Request, res:Response, next) => {
    let unauthorized = false;

    if (!req['permissions'] || !req['permissions'].length) {
      unauthorized = true;
    }

    !unauthorized && permissions.forEach(permission => {
      if (-1 === req['permissions'].indexOf(permission)) {
        unauthorized = true;
      }
    });

    if (unauthorized) {
      return res.status(HTTPStatusCodes.UNAUTHORIZED).send({});
    }

    next();
  }
}

export function hasAdminPermissions() {
  return hasPermissions(['Admin-Functions']);
}
