import {hri} from 'human-readable-ids';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';

export class BaseApiController {

  static handleAPIError(res) {
    return err => {
      const id = hri.random();
      console.error('Server error occured:', id, typeof err, err);

      let errMessage = err;
      let statusCode = HTTPStatusCodes.INTERNAL_SERVER_ERROR;
      if (typeof err === 'object') {
        if (
          (err.name && err.name.toLowerCase().indexOf('sequelize')===0) || (err.stack && err.stack.indexOf('sequelize') > -1)
        ) {
          errMessage = `Database error occured: ${id}`;
        }
        else {
          if (err.message) {
            errMessage = err.message;
          }
          if (err.statusCode) {
            statusCode = err.statusCode;
          }
        }
      }
      res.status(statusCode).json({error: errMessage});
    }
  }
}
