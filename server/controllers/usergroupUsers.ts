import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';

class UserGroupUsersController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/usergroup-users/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.User.findAll({
      include: [{
        model: db.UserGroupMapping,
        as: 'userGroups',
        required: false,
        where: {user_group_id: req.params.id},
      }],
      order: [
        ['lastname', 'ASC'],
        ['firstname', 'ASC'],
        ['createdAt', 'DESC']
      ]
    })
      .then(results => {
        if (!results) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        let processedResults = results.map(result => {
          let data = result.toJSON();
          data.on = result.userGroups.length;
          return data;
        });
        res.status(HTTPStatusCodes.OK).json(processedResults);
      })
      .catch(UserGroupUsersController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/usergroup-users
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    console.log('update usergroup users...', req.body);
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "User Group id is required"});
    }
    if (!req.body.users) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "No users specified"});
    }
    return db.UserGroupMapping.updateGroupUsers(req.params.id, req.body.users)
      .then(result => res.status(HTTPStatusCodes.CREATED).json(result))
      .catch(UserGroupUsersController.handleAPIError(res));
  }

}

export default UserGroupUsersController;
