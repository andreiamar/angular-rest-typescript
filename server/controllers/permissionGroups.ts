import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';

class PermissionGroupsController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/permission-groups
  |--------------------------------------------------------------------------
  */
  static list(req, res, next) {

    const opts = {
      order: [
        ['name', 'ASC'],
        ['createdAt', 'DESC']
      ]
    };

    return db.PermissionGroup.findAll(opts)
      .then(results => res.status(HTTPStatusCodes.OK).json(results))
      .catch(PermissionGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | GET /api/permission-groups/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.PermissionGroup.findById(req.params.id)
      .then(permission => {
        if (!permission) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        res.status(HTTPStatusCodes.OK).json(permission);
      })
      .catch(PermissionGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/permission-groups/:id
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }
    if (!req.body.name) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"Permission group name is required"});
    }

    return db.PermissionGroup.put(req.params.id, req.body.name)
      .then(() => db.PermissionGroup.findById(req.params.id))
      .then(result => res.status(HTTPStatusCodes.OK).json(result))
      .catch(PermissionGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | POST /api/permission-groups
  |--------------------------------------------------------------------------
  */
  static create(req, res, next) {
    console.log('creating permission...', req.body);
    if (!req.body.name) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Permission group name is required"});
    }
    return db.PermissionGroup.add({name: req.body.name})
      .then(result => res.status(HTTPStatusCodes.CREATED).json(result))
      .catch(PermissionGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | DELETE /api/permission/:id
  |--------------------------------------------------------------------------
  */
  static delete(req, res, next) {
    console.log('deleting permission...', req.params);
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: 'Permission group id is required'});
    }

    db.PermissionGroup.delete(req.params.id)
      .then(() => res.status(HTTPStatusCodes.OK).json({}))
      .catch(PermissionGroupsController.handleAPIError(res));
  }

}

export default PermissionGroupsController;
