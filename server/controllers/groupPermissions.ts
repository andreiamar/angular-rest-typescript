import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';

class GroupPermissionsController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/group-permissions/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.Permission.findAll({
      include: [{
        model: db.PermissionGroupMapping,
        as: 'permissionGroupMappings',
        required: false,
        where: {permission_group_id: req.params.id},
      }],
      order: [
        ['name', 'ASC'],
        ['createdAt', 'DESC']
      ]
    })
      .then(results => {
        if (!results) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        let processedResults = results.map(result => {
          let data = result.toJSON();
          data.on = result.permissionGroupMappings.length;
          return data;
        });
        res.status(HTTPStatusCodes.OK).json(processedResults);
      })
      .catch(GroupPermissionsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/group-permissions
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    console.log('update group permissions...', req.body);
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Group id is required"});
    }
    if (!req.body.permissions) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "No group permissions specified"});
    }
    return db.PermissionGroupMapping.updateGroupPermissions(req.params.id, req.body.permissions)
      .then(result => res.status(HTTPStatusCodes.CREATED).json(result))
      .catch(GroupPermissionsController.handleAPIError(res));
  }

}

export default GroupPermissionsController;
