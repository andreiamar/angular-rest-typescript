import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';

class UserGroupsController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/user-groups
  |--------------------------------------------------------------------------
  */
  static list(req, res, next) {

    const opts = {
      order: [
        ['name', 'ASC'],
        ['createdAt', 'DESC']
      ]
    };

    return db.UserGroup.findAll(opts)
      .then(results => res.status(HTTPStatusCodes.OK).json(results))
      .catch(UserGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | GET /api/user-groups/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.UserGroup.findById(req.params.id)
      .then(userGroup => {
        if (!userGroup) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        res.status(HTTPStatusCodes.OK).json(userGroup);
      })
      .catch(UserGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/user-groups/:id
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }
    if (!req.body.name) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"Name is required"});
    }

    return db.UserGroup.put(req.params.id, req.body.name)
      .then(() => db.UserGroup.findById(req.params.id))
      .then(result => res.status(HTTPStatusCodes.OK).json(result))
      .catch(UserGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | POST /api/user-groups
  |--------------------------------------------------------------------------
  */
  static create(req, res, next) {
    console.log('creating user group...', req.body);
    if (!req.body.name) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Name is required"});
    }
    return db.UserGroup.add({name: req.body.name})
      .then(result => res.status(HTTPStatusCodes.CREATED).json(result))
      .catch(UserGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | DELETE /api/permission/:id
  |--------------------------------------------------------------------------
  */
  static delete(req, res, next) {
    console.log('deleting user group...', req.params);
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: 'Id is required'});
    }

    db.UserGroup.delete(req.params.id)
      .then(() => res.status(HTTPStatusCodes.OK).json({}))
      .catch(UserGroupsController.handleAPIError(res));
  }

}

export default UserGroupsController;
