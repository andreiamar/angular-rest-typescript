import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';

class UserGroupPermissionGroupsController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/usergroup-permissiongroups/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.PermissionGroup.findAll({
      include: [{
        model: db.UserGroupPermissionMapping,
        as: 'permissionGroup',
        required: false,
        where: {user_group_id: req.params.id},
      }],
      order: [
        ['name', 'ASC'],
        ['createdAt', 'DESC']
      ]
    })
      .then(results => {
        if (!results) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        let processedResults = results.map(result => {
          let data = result.toJSON();
          data.on = result.permissionGroup.length;
          return data;
        });
        res.status(HTTPStatusCodes.OK).json(processedResults);
      })
      .catch(UserGroupPermissionGroupsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/usergroup-permissiongroups
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    console.log('update usergroup permissions...', req.body);
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "User Group id is required"});
    }
    if (!req.body.permissionGroups) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "No permissions groups specified"});
    }
    return db.UserGroupPermissionMapping.updateUserGroupPermissions(req.params.id, req.body.permissionGroups)
      .then(result => res.status(HTTPStatusCodes.CREATED).json(result))
      .catch(UserGroupPermissionGroupsController.handleAPIError(res));
  }

}

export default UserGroupPermissionGroupsController;
