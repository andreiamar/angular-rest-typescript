import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';
import * as jwt from 'jwt-simple';
import * as slug from 'slug';

import { ITag } from '../db/models/tag';

class TagsController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/tags
  |--------------------------------------------------------------------------
  */
  static list(req, res, next) {
    let total = 0;
    const perPage = 10;
    const page = req.query.page || 1;
    const offset = (page - 1) * perPage;

    const opts = {
      limit: perPage, offset: offset, order: [
        ['name', 'ASC'],
        ['createdAt', 'DESC']
      ]
    };

    return db.Tag.count({})
      .then(res => total = res)
      .then(() => db.Tag.findAll(opts))
      .then(results => res.status(HTTPStatusCodes.OK).json({
        page: page,
        perPage: perPage,
        total: total,
        data: results
      }))
      .catch(TagsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | GET /api/tags/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.Tag.findById(req.params.id)
      .then(tag => {
        if (!tag) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        res.status(HTTPStatusCodes.OK).json(tag);
      })
      .catch(TagsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/tags/:id
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }
    if (!req.body.name) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"Name is required"});
    }

    return db.Tag.findById(req.params.id)
      .then(tag => {
        if (!tag) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        return tag.update({name: slug(req.body.name).toLowerCase()})
      })
      .then(() => { return res.status(HTTPStatusCodes.OK).json({}); })
      .catch(TagsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | POST /api/tags
  |--------------------------------------------------------------------------
  */
  static create(req, res, next) {
    console.log('creating tag...', req.body);
    if (!req.body.name) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Tag name is required"});
    }
    return db.Tag.create({name: slug(req.body.name).toLowerCase()})
      .then(tag => res.status(HTTPStatusCodes.CREATED).json(tag))
      .catch(TagsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | DELETE /api/tags/:id
  |--------------------------------------------------------------------------
  */
  static delete(req, res, next) {
    console.log('deleting tag...', req.body, req.params);
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: 'Tag id is required'});
    }
    let foundTag;

    db.Tag.findById(req.params.id)
      .then(tag => {
        if (!tag) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({error: "Invalid tag"});
        }
        return tag;
      })
      .then(tag => {
        foundTag = tag;
        return db.Tag.delete(tag.id);
      })
      .then(() => res.status(HTTPStatusCodes.OK).json(foundTag))
      .catch(TagsController.handleAPIError(res));
  }

}

export default TagsController;
