import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';
import * as jwt from 'jwt-simple';

class AuthController extends BaseApiController {

  static login(req, res, next) {
    if (!req.body.email) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Email is required"});
    }
    if (!req.body.password) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Password is required"});
    }
    const queryParams = {
      where: {
        email: req.body.email
      },
      include: [{
        model: db.Persona,
        where: { is_default: true }
      }]
    };
    db.User.findOne(queryParams)
      .then(user => {
        if (!user) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({error: "Invalid credentials"});
        }
        if (!user.passwordMatches(req.body.password)) {
          return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Invalid credentials"});
        }
        return user.createJWT().then(token => {
          return res.status(HTTPStatusCodes.OK).json({token:token});
        });
      })
      .catch(AuthController.handleAPIError(res));
  }

  /**
   |--------------------------------------------------------------------------
   | Create Email and Password Account
   |--------------------------------------------------------------------------
   */
  static signup(req, res, next) {
    console.log('>>> auth/signup');
    if (!req.body.email) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Email is required"});
    }
    if (!req.body.password) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Password is required"});
    }
    if (!req.body.firstname) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "First name is required"});
    }
    if (!req.body.lastname) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Last name is required"});
    }

    return db.User.add(req.body)
      .then(res => db.User.get(res.id))
      .then(user => user.createJWT())
      .then(token => res.status(HTTPStatusCodes.CREATED).json({token: token}))
      .catch(AuthController.handleAPIError(res));
  }

  /**
   |--------------------------------------------------------------------------
   | Login with Google
   |--------------------------------------------------------------------------
   */
  static google(req, res, next) {
    let accessTokenUrl = 'https://accounts.google.com/o/oauth2/token';
    let peopleApiUrl = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect';
    let params = {
      code: req.body.code,
      client_id: req.body.clientId,
      client_secret: process.env.GOOGLE_CLIENT_SECRET,
      redirect_uri: req.body.redirectUri,
      grant_type: 'authorization_code'
    };

    // Step 1. Exchange authorization code for access token.
    request.post(accessTokenUrl, { json: true, form: params }, (err, response, token) => {
      let accessToken = token.access_token;
      let headers = { Authorization: 'Bearer ' + accessToken };

      // Step 2. Retrieve profile information about the current user.
      request.get({ url: peopleApiUrl, headers: headers, json: true }, (err, response, profile) => {
        if (profile.error) {
          return res.status(HTTPStatusCodes.INTERNAL_SERVER_ERROR).send({ error: profile.error.message});
        }
        // Step 3a. Link user accounts.
        if (req.header('Authorization')) {
          db.User.findByGoogle(profile.sub)
            .then(existingUser => {
              if (existingUser) {
                console.log(existingUser.toJSON());
                return res.status(HTTPStatusCodes.CONFLICT).send({ error: 'There is already a user associated with your Google account' });
              }
              // extract user id from Authorization header token
              let token = req.header('Authorization').split(' ')[1];
              return db.User.linkGoogle(token, profile);
            })
            .then(user => db.User.get(user.id))
            .then(user => user.createJWT())
            .then(token => res.status(HTTPStatusCodes.OK).json({token: token}))
            .catch(AuthController.handleAPIError(res));

        } else {
          console.log('>> creating new user account', profile);
          // Step 3b. Create a new user account or return an existing one.
          db.User.addGoogle(profile)
            .then(user => db.User.get(user.id))
            .then(user => user.createJWT())
            .then(token => res.status(HTTPStatusCodes.CREATED).json({token: token}))
            .catch(AuthController.handleAPIError(res));
        }
      });
    });
  }

  /**
   |--------------------------------------------------------------------------
   | Login with Facebook
   |--------------------------------------------------------------------------
   */
  static facebook(req, res, next) {
    let fields = ['id', 'email', 'first_name', 'last_name', 'link', 'name'];
    let accessTokenUrl = 'https://graph.facebook.com/v2.5/oauth/access_token';
    let graphApiUrl = 'https://graph.facebook.com/v2.5/me?fields=' + fields.join(',');
    let params = {
      code: req.body.code,
      client_id: req.body.clientId,
      client_secret: process.env.FACEBOOK_CLIENT_SECRET,
      redirect_uri: req.body.redirectUri
    };

    // Step 1. Exchange authorization code for access token.
    request.get({ url: accessTokenUrl, qs: params, json: true }, (err, response, accessToken) => {
      if (response.statusCode !== 200) {
        return res.status(HTTPStatusCodes.INTERNAL_SERVER_ERROR).send({ error: accessToken.error.message });
      }

      // Step 2. Retrieve profile information about the current user.
      request.get({ url: graphApiUrl, qs: accessToken, json: true }, (err, response, profile) => {
        if (response.statusCode !== 200) {
          return res.status(HTTPStatusCodes.INTERNAL_SERVER_ERROR).send({ error: profile.error.message });
        }
        if (!profile.picture) {
          profile.picture = `https://graph.facebook.com/v2.3/${profile.id}/picture?type=large`;
        }
        if (req.header('Authorization')) {
          db.User.findByFacebook(profile.id)
            .then(existingUser => {
              if (existingUser) {
                return res.status(HTTPStatusCodes.CONFLICT).send({ error: 'There is already a user associated with your Facebook account' });
              }

              // extract user id from Authorization header token
              let token = req.header('Authorization').split(' ')[1];
              return db.User.linkFacebook(token, profile);
            })
            .then(user => db.User.get(user.id))
            .then(user => user.createJWT())
            .then(token => res.status(HTTPStatusCodes.OK).json({token: token}))
            .catch(AuthController.handleAPIError(res));

        } else {
          // Step 3. Create a new user account or return an existing one.
          db.User.addFacebook(profile)
            .then(user => db.User.get(user.id))
            .then(user => user.createJWT())
            .then(token => res.status(HTTPStatusCodes.CREATED).json({token: token}))
            .catch(AuthController.handleAPIError(res));
        }
      });
    });
  }

  /**
   |--------------------------------------------------------------------------
   | Refresh token
   |--------------------------------------------------------------------------
   */
  static refresh(req, res, next) {
    if (!req.header('Authorization')) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Unauthorized"});
    }
    // extract user id from Authorization header token
    let token = req.header('Authorization').split(' ')[1];
    let payload = jwt.decode(token, process.env.SECRET);

    db.User.get(payload.sub)
      .then(user => {
        if (!user) {
          return res.status(HTTPStatusCodes.BAD_REQUEST).send({ error: 'User not found' });
        }
        console.log('user >>>', user.toJSON());
        user.Personas.forEach(persona => {
          console.log('persona >>>', persona.toJSON());
        })
        return user.createJWT().then(token => res.status(HTTPStatusCodes.OK).json({token: token}))
      })
      .catch(AuthController.handleAPIError(res));
  }

}

export default AuthController;
