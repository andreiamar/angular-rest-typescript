import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';
import * as jwt from 'jwt-simple';

class ProfileController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | PUT /api/profile/me
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {

    if (!req.body.email) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Email is required"});
    }
    if (!req.body.firstname) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "First name is required"});
    }
    if (!req.body.lastname) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Last name is required"});
    }
    let fields = {
      password: req.body.password,
      email: req.body.email,
      firstname: req.body.firstname,
      lastname: req.body.lastname
    };
    if (!fields.password) {
      delete fields.password;
    }

    return db.User.put(req.user, fields)
      .then(() => db.User.get(req.user))
      .then(user => user.createJWT())
      .then(token => res.status(HTTPStatusCodes.OK).json({token: token}))
      .catch(ProfileController.handleAPIError(res));
  }

}

export default ProfileController;
