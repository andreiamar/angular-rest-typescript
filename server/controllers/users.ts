import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';

// import { IArticle } from '../db/models/article';

class UsersController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/users
  |--------------------------------------------------------------------------
  */
  static list(req, res, next) {
    let total = 0;
    const perPage = 10;
    const page = req.query.page || 1;
    const offset = (page - 1) * perPage;

    let filters = {};
    let allowedFilters = [];

    allowedFilters.forEach(filter => {
      if (req.query[filter]) {
        filters[filter] = req.query[filter];
      }
    });

    const opts = {
      limit: perPage,
      offset: offset,
      order: [['createdAt', 'DESC']]
    };

    return db.User.findAll()
      .then(res => total = (res ? res.length : 0))
      .then(() => db.User.findAll(opts))
      .then(results => res.status(HTTPStatusCodes.OK).json({
        page: page,
        perPage: perPage,
        total: total,
        data: results
      }))
      .catch(UsersController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | GET /api/users/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.User.get(req.params.id)
      .then(result => {
        if (!result) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        res.status(HTTPStatusCodes.OK).json(result);
      })
      .catch(UsersController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/users/:id
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    if (!req.body.email) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Email is required"});
    }
    if (!req.body.firstname) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "First name is required"});
    }
    if (!req.body.lastname) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Last name is required"});
    }
    let fields = {
      password: req.body.password,
      email: req.body.email,
      firstname: req.body.firstname,
      lastname: req.body.lastname
    };
    if (!fields.password) {
      delete fields.password;
    }

    return db.User.put(req.user, fields)
      .then(() => db.User.get(req.user))
      .then(user => user.createJWT())
      .then(token => res.status(HTTPStatusCodes.OK).json({token: token}))
      .catch(UsersController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | DELETE /api/users/:id
  |--------------------------------------------------------------------------
  */
  static delete(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }
    return db.User.delete(req.params.id)
      .then((result) => res.status(HTTPStatusCodes.OK).json(result))
      .catch(UsersController.handleAPIError(res));
  }

}

export default UsersController;
