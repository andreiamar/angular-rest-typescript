import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';
import * as jwt from 'jwt-simple';

import { IArticle } from '../db/models/article';

class ArticlesController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/articles
  |--------------------------------------------------------------------------
  */
  static list(req, res, next) {
    let total = 0;
    const perPage = 10;
    const page = req.query.page || 1;
    const offset = (page - 1) * perPage;

    let filters = {};
    let allowedFilters = ['tag'];

    allowedFilters.forEach(filter => {
      if (req.query[filter]) {
        filters[filter] = req.query[filter];
      }
    });
    const where = {};

    // check permissions
    // let isAuthorized = false;
    // if (req['permissions'].indexOf('Read-Public-Article') > -1) {
    //   isAuthorized = true;
    // }
    // else if (req['permissions'].indexOf('Read-Own-Article') > -1) {
    //     // only read own articles
    //     where['user_id'] = req['user'];
    //     isAuthorized = true;
    // }
    // if (!isAuthorized) {
    //   return res.status(HTTPStatusCodes.UNAUTHORIZED).json({});
    // }

    const opts = {
      where: where,
      include: [
        {
          model: db.ArticleTagMapping,
          as: 'tags',
          include: [
            {
              model: db.Tag,
              as: 'tag',
              where: filters['tag'] ? {
                name: filters['tag']
              } : {}
            }
          ]
        },
        {
          model: db.User,
          as: 'author'
        },
      ],
      limit: perPage, offset: offset, order: [['createdAt', 'DESC']]
    };

    let countOpts = (<any>Object).assign({}, opts);
    countOpts['distinct'] = 'id';
    delete countOpts['limit'];
    delete countOpts['offset'];

    return db.Article.count(countOpts)
      .then(res => total = res)
      .then(() => db.Article.findAll(opts))
      .then(articles => res.status(HTTPStatusCodes.OK).json({
        page: page,
        perPage: perPage,
        total: total,
        data: articles
      }))
      .catch(ArticlesController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | GET /api/articles/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.Article.get(req.params.id)
      .then(article => {
        if (!article) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }

        // check permissions
        // let isAuthorized = false;
        // if (req['permissions'].indexOf('Read-Public-Article') > -1) {
        //   isAuthorized = true;
        // }
        // else if (req['permissions'].indexOf('Read-Own-Article') > -1) {
        //   isAuthorized = (article.user_id == req['user']);
        // }
        // if (!isAuthorized) {
        //   return res.status(HTTPStatusCodes.UNAUTHORIZED).json({});
        // }

        res.status(HTTPStatusCodes.OK).json(article);
      })
      .catch(ArticlesController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | POST /api/articles
  |--------------------------------------------------------------------------
  */
  static create(req, res, next) {
    // check permissions
    if (req['permissions'].indexOf('Create-Article') === -1) {
      return res.status(HTTPStatusCodes.UNAUTHORIZED).json({});
    }

    if (!req.body.title) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Article title is required"});
    }
    if (!req.body.body) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Article body is required"});
    }

    const article:IArticle = {
      user_id: req.user,
      title: req.body.title,
      body: req.body.body
    };
    let tags = [];
    if (req.body.tags && req.body.tags.length) {
      tags = req.body.tags.filter(Boolean);
    }
    return db.Article.add(article, tags)
      .then(article => res.status(HTTPStatusCodes.CREATED).json(article))
      .catch(ArticlesController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/articles/:id
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }
    if (!req.body.title) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Article title is required"});
    }
    if (!req.body.body) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Article body is required"});
    }

    db.Article.get(req.params.id)
      .then(article => {
        if (!article) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }

        // check permissions
        let isAuthorized = false;
        if (req['permissions'].indexOf('Edit-All-Articles') > -1) {
          isAuthorized = true;
        }
        else if (req['permissions'].indexOf('Edit-Own-Article') > -1) {
          isAuthorized = (article.user_id == req['user']);
        }
        if (!isAuthorized) {
          return res.status(HTTPStatusCodes.UNAUTHORIZED).json({});
        }

        const fields:IArticle = {
          user_id: req.user,
          title: req.body.title,
          body: req.body.body
        };
        let tags = [];
        if (req.body.tags && req.body.tags.length) {
          tags = req.body.tags.filter(Boolean);
        }
        return db.Article.put(req.params.id, fields, tags)
          .then(() => db.Article.get(req.params.id))
          .then((updatedArticle) => res.status(HTTPStatusCodes.CREATED).json(updatedArticle));
      })
      .catch(ArticlesController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | DELETE /api/articles/:id
  |--------------------------------------------------------------------------
  */
  static delete(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    db.Article.get(req.params.id)
      .then(article => {
        if (!article) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }

        // check permissions
        let isAuthorized = false;
        if (req['permissions'].indexOf('Delete-All-Articles') > -1) {
          isAuthorized = true;
        }
        else if (req['permissions'].indexOf('Delete-Own-Article') > -1) {
          isAuthorized = (article.user_id == req['user']);
        }
        if (!isAuthorized) {
          return res.status(HTTPStatusCodes.UNAUTHORIZED).json({});
        }

        return db.Article.delete(req.params.id)
          .then((article) => res.status(HTTPStatusCodes.OK).json(article));
      })
      .catch(ArticlesController.handleAPIError(res));
  }

}

export default ArticlesController;
