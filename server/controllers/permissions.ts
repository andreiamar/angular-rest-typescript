import * as express from 'express';
import db from '../db/models';
import { HTTPStatusCodes } from '../lib/httpStatusCodes';
import { BaseApiController } from './base';
import * as request from 'request';

class PermissionsController extends BaseApiController {

  /*
  |--------------------------------------------------------------------------
  | GET /api/permissions
  |--------------------------------------------------------------------------
  */
  static list(req, res, next) {

    const opts = {
      order: [
        ['name', 'ASC'],
        ['createdAt', 'DESC']
      ]
    };

    return db.Permission.findAll(opts)
      .then(results => res.status(HTTPStatusCodes.OK).json(results))
      .catch(PermissionsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | GET /api/permissions/:id
  |--------------------------------------------------------------------------
  */
  static get(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }

    return db.Permission.findById(req.params.id)
      .then(permission => {
        if (!permission) {
          return res.status(HTTPStatusCodes.NOT_FOUND).json({});
        }
        res.status(HTTPStatusCodes.OK).json(permission);
      })
      .catch(PermissionsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | PUT /api/permissions/:id
  |--------------------------------------------------------------------------
  */
  static update(req, res, next) {
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"ID not specified"});
    }
    if (!req.body.name) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error:"Name is required"});
    }

    return db.Permission.put(req.params.id, req.body.name)
      .then(() => db.Permission.findById(req.params.id))
      .then(result => res.status(HTTPStatusCodes.OK).json(result))
      .catch(PermissionsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | POST /api/permissions
  |--------------------------------------------------------------------------
  */
  static create(req, res, next) {
    console.log('creating permission...', req.body);
    if (!req.body.name) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: "Permission name is required"});
    }
    return db.Permission.add({name: req.body.name})
      .then(result => res.status(HTTPStatusCodes.CREATED).json(result))
      .catch(PermissionsController.handleAPIError(res));
  }

  /*
  |--------------------------------------------------------------------------
  | DELETE /api/permission/:id
  |--------------------------------------------------------------------------
  */
  static delete(req, res, next) {
    console.log('deleting permission...', req.params);
    if (!req.params.id) {
      return res.status(HTTPStatusCodes.BAD_REQUEST).json({error: 'Permission id is required'});
    }

    db.Permission.delete(req.params.id)
      .then(() => res.status(HTTPStatusCodes.OK).json({}))
      .catch(PermissionsController.handleAPIError(res));
  }

}

export default PermissionsController;
