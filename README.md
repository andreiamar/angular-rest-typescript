# cottus-io

Collaboration engine MVP

## A. Development

### Prerequisites

The following packages should be installed globally: `npm`, `Node.js`, `Angular CLI`

### Installation

- Create a local postgres database

- Copy `.env.example` to `.env` and fill in the environment variables

- Copy `client/src/environments/environment.sample.ts` to `client/src/environments/environment.ts` and fill in with the appropriate values

- Run `npm install` to install dependencies

- Run `npm run start:dev` to start the server process. The server will accept connections at `https://localhost:3000/`, unless the `PORT` environment variable is set to another value. Test the server url - it should display a 404 Error (the server does not serve the frontend app in development)

- Install the client dependencies: `cd client && npm install`

- Serve the client app: `npm run serve:client`. The Angular development server url will be displayed in the console (the default is `http://localhost:4200`)

## B. Production

### Prerequisites

The following Config Vars must be set in Heroku: `DATABASE_URL`, `FACEBOOK_CLIENT_SECRET`, `GOOGLE_CLIENT_SECRET`, `NODE_ENV`=`production`, `PGSSLMODE`=`require`, `SECRET`, `SYNC_DB`=`false`, `ADMIN_EMAIL`=`admin@cottus.io`, `ADMIN_PASSWORD`=`cottus`, `ADMIN_FIRSTNAME`=`Admin`, `ADMIN_LASTNAME`=`System`

Add a remote named `heroku` to the local git project: `git remote add heroku https://git.heroku.com/cottus-io.git`

### Installation

- Update the Facebook app settings and add `https://cottus-io.herokuapp.com` as the Site URL

- Update the Google app settings and add `https://cottus-io.herokuapp.com` as the Authorized JavaScript origins and `https://cottus-io.herokuapp.com/auth/google/callback` as the Authorized redirect URI

- Set the SYNC_DB Heroku config variable to 'true' to synchronize the db, and restart the dyno running the app to apply the sync

- Deploy the app's current branch by running `git push -f heroku HEAD:master`
